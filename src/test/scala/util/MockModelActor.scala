package util

import akka.actor.Actor
import shapeless.TypeCase

class MockModelActor(map: Map[String, Any])extends Actor{
  val MapOfStringAny    = TypeCase[Map[String,Any]]
  override def receive: Receive = {
    case "getModel" => sender() ! map
    case MapOfStringAny(a) => sender() ! a
    case "failGetModel" => context.become(failGetModel)
    case "failModify" => context.become(failModify)
    case "normal" => context.become(receive)
  }

  def failGetModel:Receive={
    case "getModel" => //sender() ! new AskTimeoutException("")//sender() ! new Failure[String](new Exception)
    case MapOfStringAny(a) => sender() ! a
    case "failGetModel" => context.become(failGetModel)
    case "failModify" => context.become(failModify)
    case "normal" => context.become(receive)
  }

  def failModify:Receive={
    case "getModel" => sender() ! map
    case MapOfStringAny(a) => //sender() ! a
    case "failGetModel" => context.become(failGetModel)
    case "failModify" => context.become(failModify)
    case "normal" => context.become(receive)
  }
}