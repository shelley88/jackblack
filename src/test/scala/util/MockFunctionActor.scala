package util

import akka.actor.Actor
import shapeless.TypeCase

class MockFunctionActor extends Actor{
  val MapOfStringAny    = TypeCase[Map[String,Any]]
  override def receive: Receive = {
    case (MapOfStringAny(function),MapOfStringAny(input),MapOfStringAny(allFunctions)) => sender() ! 7
  }

}
