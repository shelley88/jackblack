//package api
//
//import akka.http.scaladsl.model._
//import akka.http.scaladsl.model.headers.RawHeader
//import akka.http.scaladsl.testkit.ScalatestRouteTest
////import com.datastax.driver.core.{Cluster, Session}
////import io.getquill.{CassandraAsyncContext, SnakeCase}
//import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
////import org.cassandraunit.utils.EmbeddedCassandraServerHelper
////import org.cassandraunit.CQLDataLoader
////import org.cassandraunit.dataset.cql.ClassPathCQLDataSet
//
//
//
//
//class WebServerSpec  extends WordSpec with BeforeAndAfterAll with Matchers with ScalatestRouteTest {
////  object InitializeTests {
////
////    lazy val clusterWithoutSSL =
////      Cluster.builder()
////        .withPort(9043)
////        .addContactPoint("localhost")
////        .withCredentials("cassandra", "cassandra").build()
////
////    lazy val ctx = new CassandraAsyncContext[SnakeCase](
////      naming = SnakeCase,
////      cluster = clusterWithoutSSL,
////      keyspace = "twittersentiment",
////      preparedStatementCacheSize = 100
////    )
////
////    def initializeKeyspaces() = {
////      val dataLoader = new CQLDataLoader(clusterWithoutSSL.connect())
////      dataLoader.load(new ClassPathCQLDataSet("twittersentiment.cql", true, "twittersentiment"))
////    }
//
//
////  var cluster: Cluster = _
////  var session: Session = _
////
////  override def beforeAll(): Unit = {
////    EmbeddedCassandraServerHelper.startEmbeddedCassandra("file:///cu-cassandra.yaml",6000l)
////    cluster = EmbeddedCassandraServerHelper.getCluster
////    session = cluster.connect()
////    loadData()
////  }
//////  }
////
////  def loadData(): Unit = {
////    val dataLoader = new CQLDataLoader(session)
////    dataLoader.load(new ClassPathCQLDataSet("simple.cql", "hmda_query"))
////  }
//
//  "The service " must{
//    "get empty sessions" in {
//      Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "ParSet()"
//      }
//    }
////    "BlackJack example should work" in{
////      HttpRequest(HttpMethods.POST,uri="/runSimulation").withHeaders(List(RawHeader("concurrent","1"),RawHeader("simulation","BlackJack-simple")))~> WebServer.routes ~> check{
////        responseAs[String] shouldEqual "ParVector((1,Map(playerWins -> 0, gameStage -> 6, gameNumber -> 5.0, playerPoints -> Map(hard -> 0, soft -> 0), maxGames -> 5, dealerHand -> List(), playerHand -> List(), turn -> dealer, shoe -> List(), dealerWins -> 5.0, dealerPoints -> Map(hard -> 0, soft -> 0))))"
////      }
////    }
//    "setModel should create session and that session should be destroyed" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("gameName","gameName")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "ParSet("+session+")"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//      Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "ParSet()"
//      }
//    }
//    "should not stop a non existent session" in {
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//    }
////    "should not start a session twice" in {
////      Get("/sessions") ~> WebServer.routes ~> check{
////        responseAs[String] shouldEqual "ParSet()"
////      }
////      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1"),RawHeader("gameName","gameName")))~> WebServer.routes ~> check{
////        responseAs[String] shouldEqual "Model Set"
////      }
////      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1"),RawHeader("gameName","gameName")))~> WebServer.routes ~> check{
////        responseAs[String] shouldEqual "Model Already Set, dumb ass!!!"
////      }
////      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
////        responseAs[String] shouldEqual "Simulation stopped"
////      }
////    }
//    "should set/get model correctly, set functions rules and runner, run rules and runner , update model" in {
//
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}]}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "ParSet("+session+")"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isFunc,1), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRunner").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((stop,true))"
//      }
//      HttpRequest(HttpMethods.GET,uri="/getModel").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Map(hand1 -> 3, hand2 -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), table -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/updateModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":7,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}]}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Updated"
//      }
//      HttpRequest(HttpMethods.GET,uri="/getModel").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Map(hand1 -> 7, hand2 -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), table -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//    "should fail when session not started" in {
//      HttpRequest(HttpMethods.POST, uri = "/updateModel", entity = HttpEntity(MediaTypes.`application/json`, "{}")).withHeaders(List(RawHeader("session", "1"))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.GET,uri="/getModel").withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRunner").withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/modifyRules",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/modifyFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/modifyRunner",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/addRules",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/addFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/addRunner",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/deleteRules",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/deleteFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//      HttpRequest(HttpMethods.POST,uri="/deleteRunner",entity = HttpEntity(MediaTypes.`application/json`,"{}")).withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "You don't have a session started, dumb ass!!!"
//      }
//
//    }
//    "should modify rules" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isFunc,1), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/modifyRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"modifyRules\":{\n\t\"isWinner1\":{\"type\":\"value\",\"value\":\"Modified\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Modified"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Modified), (isOperation,true), (isFunc,1), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//    "should add rules" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isFunc,1), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/addRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"addRules\":{\n\t\"lala2\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Added"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isFunc,1), (isWinner2,1), (lala2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//    "should delete rules" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isFunc,1), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/deleteRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"deleteRules\":[\"isFunc\"]}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Deleted"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//    "should modify runner" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/modifyRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"modifyRunner\":{\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":5}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":5},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Modified"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRunner").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((stop,true))"
//      }
//      HttpRequest(HttpMethods.GET,uri="/getModel").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Map(hand1 -> 5, hand2 -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), lolo -> 0, table -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), lala -> 3)"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//
//    "should add runner" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/addRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"addRunner\":{\n\t\"lolo\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lolo\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lolo\",\"value\":{\"type\":\"value\",\"value\":2}}] }}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Added"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRunner").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((stop,true))"
//      }
//      HttpRequest(HttpMethods.GET,uri="/getModel").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Map(hand1 -> 3, hand2 -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), lolo -> 2, table -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), lala -> 3)"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//
//    "should delete runner" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/deleteRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"deleteRunner\":[\"lala\"]}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Deleted"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRunner").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((stop,true))"
//      }
//      HttpRequest(HttpMethods.GET,uri="/getModel").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Map(hand1 -> 3, hand2 -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), lolo -> 0, table -> List(Map(val1 -> 1, val2 -> 2), Map(val1 -> 1, val2 -> 2)), lala -> 0)"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//
//    "should modify function" in {
//      HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST,uri="/modifyFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"modifyFunctions\":{\n\"func1\":{\"type\":\"value\",\"value\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Functions Modified"
//      }
//      HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isFunc,lala), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//
//    "should add function" in {
//      HttpRequest(HttpMethods.POST, uri = "/setModel", entity = HttpEntity(MediaTypes.`application/json`, "{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName", "1"))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "Model Set"
//      }
//      val t=Get("/sessions") ~> WebServer.routes ~> check{
//        responseAs[String]
//      }
//      val session=t.replace("ParSet(","").replace(")","")
//      HttpRequest(HttpMethods.POST, uri = "/setFunctions", entity = HttpEntity(MediaTypes.`application/json`, "{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session", session))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "Functions Set"
//      }
//      HttpRequest(HttpMethods.POST, uri = "/setRules", entity = HttpEntity(MediaTypes.`application/json`, "{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session", session))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "Rules Set"
//      }
//      HttpRequest(HttpMethods.POST, uri = "/setRunner", entity = HttpEntity(MediaTypes.`application/json`, "{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session", session))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "Runner Set"
//      }
//      HttpRequest(HttpMethods.POST, uri = "/modifyRules", entity = HttpEntity(MediaTypes.`application/json`, "{\"modifyRules\":{\n\t\"isFunc\":{\"type\":\"function\",\"function\":\"func2\",\"input\":{\"lala\":1}}}}")).withHeaders(List(RawHeader("session", session))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "Rules Modified"
//      }
//      HttpRequest(HttpMethods.POST, uri = "/addFunctions", entity = HttpEntity(MediaTypes.`application/json`, "{\"addFunctions\":{\n\"func2\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session", session))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "Functions Added"
//      }
//      HttpRequest(HttpMethods.POST, uri = "/runRules").withHeaders(List(RawHeader("session", session))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "List((isOperationEmbedded,true), (isWinner1,Equals), (isOperation,true), (isFunc,1), (isWinner2,1))"
//      }
//      HttpRequest(HttpMethods.POST, uri = "/stop").withHeaders(List(RawHeader("session", session))) ~> WebServer.routes ~> check {
//        responseAs[String] shouldEqual "Simulation stopped"
//      }
//    }
//
//      "should delete function" in {
//        HttpRequest(HttpMethods.POST,uri="/setModel",entity = HttpEntity(MediaTypes.`application/json`,"{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"lala\":0,\n\"lolo\":0\n}")).withHeaders(List(RawHeader("gameName","1")))~> WebServer.routes ~> check{
//          responseAs[String] shouldEqual "Model Set"
//        }
//        val t=Get("/sessions") ~> WebServer.routes ~> check{
//          responseAs[String]
//        }
//        val session=t.replace("ParSet(","").replace(")","")
//        HttpRequest(HttpMethods.POST,uri="/setFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//          responseAs[String] shouldEqual "Functions Set"
//        }
//        HttpRequest(HttpMethods.POST,uri="/setRules",entity = HttpEntity(MediaTypes.`application/json`,"{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//          responseAs[String] shouldEqual "Rules Set"
//        }
//        HttpRequest(HttpMethods.POST,uri="/setRunner",entity = HttpEntity(MediaTypes.`application/json`,"{\"runner\":{\n\t\"make1-2inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\n\t\"make2-3inhand1\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"lala\":{\n\t\t\"rule\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"path\",\"path\":\"lala\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"lala\",\"value\":{\"type\":\"value\",\"value\":3}}] },\n\t\"stop\":{\n\t\t\"stop-condition\":{\n\t\t\t\"type\":\"operation\",\n\t\t\t\"operator\":\"Equals\",\n\t\t\t\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//          responseAs[String] shouldEqual "Runner Set"
//        }
//        HttpRequest(HttpMethods.POST,uri="/deleteFunctions",entity = HttpEntity(MediaTypes.`application/json`,"{\"deleteFunctions\":[\"func1\"]}")).withHeaders(List(RawHeader("session",session)))~> WebServer.routes ~> check{
//          responseAs[String] shouldEqual "Functions Deleted"
//        }
//        //TODO when errors are handled properly
////        HttpRequest(HttpMethods.POST,uri="/runRules").withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
////          responseAs[String] shouldEqual "List((isFunc,1), (isOperation,true), (isOperationEmbedded,true), (isWinner1,Equals), (isWinner2,1))"
////        }
////        HttpRequest(HttpMethods.POST,uri="/stop").withHeaders(List(RawHeader("session","1")))~> WebServer.routes ~> check{
////          responseAs[String] shouldEqual "Simulation stopped"
////        }
//      }
//    }
//}
