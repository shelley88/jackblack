package runner

import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.testkit.TestKit
import akka.util.Timeout
import org.scalatest.concurrent.PatienceConfiguration
import org.scalatest.{Matchers, WordSpecLike}
import util.MockModelActor

import scala.concurrent.Await
import scala.concurrent.duration._

class MorphActorSpec extends TestKit(ActorSystem("AkkaTestSystem")) with WordSpecLike with Matchers {
  implicit val timeout: Timeout = Timeout(7 seconds)

  val modelActor = system.actorOf(Props(new MockModelActor(Map("hand1" -> "lalal", "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2))))),"Model")
  val model = Map("hand1" -> "lalal", "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
  val morphActor=system.actorOf(Props(new MorphActor(modelActor)),"morphActor")

  "A MorphActor" must{
    "should add element to model in path" in{
      val wait = morphActor ? ("add", List("hand2"), Map("val3" -> 9),model)
      val result = Await.result(wait,1000 millis)
      result shouldEqual Map("hand1" -> "lalal", "hand2" -> List(Map("val3" -> 9), Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "should addWithoutFlatten element to model in path" in{
      val wait = morphActor ? ("addWithoutFlatten", List("hand2"), List(Map("val3" -> 9)),model)
      val result = Await.result(wait,1000 millis)
      result shouldEqual Map("hand1" -> "lalal", "hand2" -> List(List(Map("val3" -> 9)), Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "should remove element from model in path" in{
      val wait = morphActor ? ("remove", List("hand2"), Map("val1" -> 1, "val2" -> 2),model)
      val result = Await.result(wait,1000 millis)
      result shouldEqual Map("hand1" -> "lalal", "hand2" -> List(), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "should change element in model in path" in{
      val wait = morphActor ? ("change", List("hand2"), List(Map("val1" -> 3, "val2" -> 2)),model)
      val result = Await.result(wait,1000 millis)
      result shouldEqual Map("hand1" -> "lalal", "hand2" -> List(Map("val1" -> 3, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "should fail properly when not valid data is sent to morph actor" in{
      val wait = morphActor ? 7
      val result = Await.result(wait, 1000 millis)
      result shouldEqual "morphActor not matching 7"
    }

//    "should fail add properly when get model times out" in{
//      modelActor ! "failGetModel"
//      val wait = morphActor ? ("add", List("hand2"), 7)
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
//
//    "should fail add properly when modify model times out" in{
//      modelActor ! "failModify"
//      val wait = morphActor ? ("add", List("hand2"), 7)
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
//
//    "should fail addWithoutFlatten properly when get model times out" in{
//      modelActor ! "failGetModel"
//      val wait = morphActor ? ("add", List("hand2"), 7)
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
//
//    "should fail addWithoutFlatten properly when modify model times out" in{
//      modelActor ! "failModify"
//      val wait = morphActor ? ("addWithoutFlatten", List("hand2"), 7)
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
//
//    "should fail remove properly when get model times out" in{
//      modelActor ! "failGetModel"
//      val wait = morphActor ? ("remove", List("hand2"), List(Map("val1" -> 3, "val2" -> 2)))
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
//
//    "should fail remove properly when modify model times out" in{
//      modelActor ! "failModify"
//      val wait = morphActor ? ("remove", List("hand2"), 7)
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
//    "should fail change properly when get model times out" in{
//      modelActor ! "failGetModel"
//      val wait = morphActor ? ("change", List("hand2"), List(Map("val1" -> 3, "val2" -> 2)))
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
//
//    "should fail change properly when modify model times out" in{
//      modelActor ! "failModify"
//      val wait = morphActor ? ("change", List("hand2"), 7)
//      val result = Await.result(wait, 6000 millis)
//      val ex = new akka.pattern.AskTimeoutException("")
//      result.getClass shouldEqual ex.getClass
//      modelActor ! "normal"
//    }
  }
}


