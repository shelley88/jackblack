import runner.ActionActor
import akka.actor.{ActorRef, ActorSelection, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.scalatest.{Matchers, WordSpecLike}
import runner.RunnerActor

import scala.concurrent.Await
import scala.concurrent.duration._

class UserStoryTest extends WordSpecLike with Matchers {
  val mapper = new ObjectMapper
  mapper.registerModule(DefaultScalaModule)

  val inputJSON = "{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}]}"
  val inputJSONA = "{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\"isFunc\":0}"
  val modelA: Map[String, Any] = mapper.readValue(inputJSONA, classOf[Map[String, Any]])
  val model: Map[String, Any] = mapper.readValue(inputJSON, classOf[Map[String, Any]])
  val rules: Map[String, Any] = mapper.readValue("{\"rules\":{\n\"isFunc\":{\"type\":\"function\",\"function\":\"func1\",\"input\":{\"lala\":1}},\t\n\"isWinner1\":{\"type\":\"value\",\"value\":\"Equals\"},\n\"isWinner2\":{\"type\":\"path\",\"path\":\"hand1\"},\n\"isOperation\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\n\"isOperationEmbedded\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":true},{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":0},{\"type\":\"value\",\"value\":0}]}]}}}", classOf[Map[String, Any]])
  val functions: Map[String, Any] = mapper.readValue("{\"functions\":{\n\"func1\":{\"type\":\"input\",\"input\":\"lala\"}}}", classOf[Map[String, Any]])
  val runnerInfo:Map[String,Any] = mapper.readValue("{\"runner\":{\"make1-2inhand1\":{\"rule\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":1},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":2}}] },\"make2-3inhand1\":{\"rule\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":2},{\"type\":\"path\",\"path\":\"hand1\"}]},\"side-effects\":[{\"type\":\"change\",\"path\":\"hand1\",\"value\":{\"type\":\"value\",\"value\":3}}] },\"stop\":{\"stop-condition\":{\"type\":\"operation\",\"operator\":\"Equals\",\"parameters\":[{\"type\":\"value\",\"value\":3},{\"type\":\"path\",\"path\":\"hand1\"}]}}}}",classOf[Map[String,Any]])
  val runnerInfoA:Map[String,Any] = mapper.readValue("{  \n   \"runner\":{  \n      \"usesRules\":{  \n         \"rule\":{  \n            \"type\":\"operation\",\n            \"operator\":\"Equals\",\n            \"parameters\":[  \n               {  \n                  \"type\":\"value\",\n                  \"value\":1\n               },\n               {  \n                  \"type\":\"path\",\n                  \"path\":\"hand1\"\n               }\n            ]\n         },\n         \"side-effects\":[  \n            {  \n               \"type\":\"change\",\n               \"path\":\"isFunc\",\n               \"value\":{  \n                  \"type\":\"rule\",\n                  \"rule\":\"isFunc\"\n               }\n            }\n         ]\n      },\"make1-2inhand1\":{  \n         \"rule\":{  \n            \"type\":\"operation\",\n            \"operator\":\"Equals\",\n            \"parameters\":[  \n               {  \n                  \"type\":\"value\",\n                  \"value\":1\n               },\n               {  \n                  \"type\":\"path\",\n                  \"path\":\"hand1\"\n               }\n            ]\n         },\n         \"side-effects\":[  \n            {  \n               \"type\":\"change\",\n               \"path\":\"hand1\",\n               \"value\":{  \n                  \"type\":\"value\",\n                  \"value\":2\n               }\n            }\n         ]\n      },\n      \"make2-3inhand1\":{  \n         \"rule\":{  \n            \"type\":\"operation\",\n            \"decision\":true,\n            \"decisionValue\":1,\n            \"operator\":\"Equals\",\n            \"parameters\":[  \n               {  \n                  \"type\":\"value\",\n                  \"value\":2\n               },\n               {  \n                  \"type\":\"path\",\n                  \"path\":\"hand1\"\n               }\n            ]\n         },\n         \"decisionValue\":{\n         \"type\":\"value\",\n         \"value\":0\n         },\n         \"side-effects\":[  \n            {  \n               \"type\":\"change\",\n               \"path\":\"hand1\",\n               \"value\":{  \n                  \"type\":\"value\",\n                  \"value\":3\n               }\n            }\n         ]\n      },\n      \"make2-4inhand1\":{  \n         \"rule\":{  \n            \"type\":\"operation\",\n            \"decision\":true,\n            \"operator\":\"Equals\",\n            \"parameters\":[  \n               {  \n                  \"type\":\"value\",\n                  \"value\":2\n               },\n               {  \n                  \"type\":\"path\",\n                  \"path\":\"hand1\"\n               }\n            ]\n         },\n         \"decisionValue\":{\n         \"type\":\"value\",\n         \"value\":1\n         },\n         \"side-effects\":[  \n            {  \n               \"type\":\"change\",\n               \"path\":\"hand1\",\n               \"value\":{  \n                  \"type\":\"value\",\n                  \"value\":4\n               }\n            }\n         ]\n      },\n      \"stop\":{  \n         \"stop-condition\":{  \n            \"type\":\"operation\",\n            \"operator\":\"LessOrEquals\",\n            \"parameters\":[  \n               {  \n                  \"type\":\"value\",\n                  \"value\":3\n               },\n               {  \n                  \"type\":\"path\",\n                  \"path\":\"hand1\"\n               }\n            ]\n         }\n      }\n   }\n}",classOf[Map[String,Any]])


  val system2 = ActorSystem("testsystem2")
  implicit val timeout: Timeout = Timeout(5 seconds)
  val action: ActorRef = system2.actorOf(Props(new ActionActor("/user/*/Model")), "ActionActor")

  val morphA: ActorSelection = system2.actorSelection(action.path+"/Morph")
  val modelActorA: ActorSelection = system2.actorSelection(action.path + "/Model")
  val system = ActorSystem("testsystem")
  val runner: ActorRef = system.actorOf(Props(new RunnerActor("/user/*/Model")), "RunnerActor")

  val morph: ActorSelection = system.actorSelection(runner.path+"/Morph")
  val modelActor: ActorSelection = system.actorSelection(runner.path + "/Model")

  "An Echo actor" must {
    "setup correctly" in {
      runner ! model

      runner ! functions("functions")
      runner ! rules("rules")
      runner ! runnerInfo("runner")

      val getModel = runner ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))

      val runRules = runner ? "run"
      val runRulesResult = runRules.mapTo[List[Map[String, Any]]]
      val runRulesWait = Await.result(runRulesResult, 1000 millis)
      runRulesWait shouldEqual List(("isOperationEmbedded",true), ("isWinner1","Equals"), ("isOperation",true), ("isFunc",1), ("isWinner2",1))
    }

    "change value" in {
      val changeModel = morph ? ("change", List("hand1"), 2,model)
      val changeModelResult = changeModel.mapTo[Map[String, Any]]
      val changeModelWait = Await.result(changeModelResult, 1000 millis)
      changeModelWait shouldEqual Map("hand1" -> 2, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))

      val getModel = modelActor ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> 2, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "change again" in {
      val changeModelAgain = morph ? ("change", List("hand1"), List(Map("card1" -> 7)),model)
      val changeModelAgainResult = changeModelAgain.mapTo[Map[String, Any]]
      val changeModelAgainWait = Await.result(changeModelAgainResult, 1000 millis)
      changeModelAgainWait shouldEqual Map("hand1" -> List(Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))

      val getModel = modelActor ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> List(Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "add" in {
      val addModel = morph ? ("add", List("hand1"), List(Map("card2" -> 9)),Map("hand1" -> List(Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2))))
      val addModelResult = addModel.mapTo[Map[String, Any]]
      val addModelWait = Await.result(addModelResult, 1000 millis)
      addModelWait shouldEqual Map("hand1" -> List(Map("card2" -> 9), Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))

      val getModel = modelActor ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> List(Map("card2" -> 9), Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "remove" in {
      val removeModel = morph ? ("remove", List("hand1"), Map("card2" -> 9),Map("hand1" -> List(Map("card2" -> 9), Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2))))
      val removeModelResult = removeModel.mapTo[Map[String, Any]]
      val removeModelWait = Await.result(removeModelResult, 1000 millis)
      removeModelWait shouldEqual Map("hand1" -> List(Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))

      val getModel = modelActor ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> List(Map("card1" -> 7)), "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "runner" in {
      val changeModel = morph ? ("change", List("hand1"), 1,model)
      val changeModelResult = changeModel.mapTo[Map[String, Any]]
      val changeModelWait = Await.result(changeModelResult, 1000 millis)
      changeModelWait shouldEqual Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))

      val getModel2 = modelActor ? "getModel"
      val getModel2Result = getModel2.mapTo[Map[String, Any]]
      val getModel2Wait = Await.result(getModel2Result, 1000 millis)
      getModel2Wait shouldEqual Map("hand1" ->1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))

      val runRules = runner ? "run"
      val runRulesResult = runRules.mapTo[List[Map[String, Any]]]
      val runRulesWait = Await.result(runRulesResult, 1000 millis)
      runRulesWait shouldEqual List(("isOperationEmbedded",true), ("isWinner1","Equals"), ("isOperation",true), ("isFunc",1), ("isWinner2",1))

      val runnerRun=runner ? "run simulation"
      val runnerWait = Await.result(runnerRun,5000 millis)
      runnerWait shouldEqual List(("stop",true))

      val getModel = modelActor ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> 3, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "action" in {
      action ! modelA

      action ! functions("functions")
      action ! rules("rules")
      action ! runnerInfoA("runner")
      val changeModel = morphA ? ("change", List("hand1"), 1,model)
      val changeModelResult = changeModel.mapTo[Map[String, Any]]
      val changeModelWait = Await.result(changeModelResult, 1000 millis)
      changeModelWait shouldEqual Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)),"isFunc"->0)

      val getModel2 = modelActorA ? "getModel"
      val getModel2Result = getModel2.mapTo[Map[String, Any]]
      val getModel2Wait = Await.result(getModel2Result, 1000 millis)
      getModel2Wait shouldEqual Map("hand1" ->1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)),"isFunc"->0)

      val runRules = action ? "run"
      val runRulesResult = runRules.mapTo[List[Map[String, Any]]]
      val runRulesWait = Await.result(runRulesResult, 1000 millis)
      runRulesWait shouldEqual List(("isOperationEmbedded",true), ("isWinner1","Equals"), ("isOperation",true), ("isFunc",1), ("isWinner2",1))

      val runnerRun=action ? "run simulation"
      val runnerWait = Await.result(runnerRun,5000 millis)
      runnerWait shouldEqual List(("stop",true,false))

      val getModel = modelActorA ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> 4, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "isFunc" -> 1)
    }

    "action with composed operation" in {
//      action ! modelA
//
//      action ! functions("functions")
//      action ! rules("rules")
//      action ! runnerInfoA("runner")
      val changeModel = morphA ? ("change", List("hand1"), 1,model)
      val changeModelResult = changeModel.mapTo[Map[String, Any]]
      val changeModelWait = Await.result(changeModelResult, 1000 millis)
      changeModelWait shouldEqual Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)),"isFunc"->1)

      val getModel2 = modelActorA ? "getModel"
      val getModel2Result = getModel2.mapTo[Map[String, Any]]
      val getModel2Wait = Await.result(getModel2Result, 1000 millis)
      getModel2Wait shouldEqual Map("hand1" ->1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)),"isFunc"->1)

      val runRules = action ? "run"
      val runRulesResult = runRules.mapTo[List[Map[String, Any]]]
      val runRulesWait = Await.result(runRulesResult, 1000 millis)
      runRulesWait shouldEqual List(("isOperationEmbedded",true), ("isWinner1","Equals"), ("isOperation",true), ("isFunc",1), ("isWinner2",1))

      val runnerRun=action ? "run simulation"
      val runnerWait = Await.result(runnerRun,5000 millis)
      runnerWait shouldEqual List(("stop",true,false))

      val getModel = modelActorA ? "getModel"
      val getModelResult = getModel.mapTo[Map[String, Any]]
      val getModelWait = Await.result(getModelResult, 1000 millis)
      getModelWait shouldEqual Map("hand1" -> 4, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "isFunc" -> 1)
    }
  }
}