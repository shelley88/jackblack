package engine.rules

import org.scalatest.{Matchers, WordSpec}

class GenericLambdaSpec extends WordSpec with Matchers{

  "A lambda" must {
    //findElement
    "should findElement in first level" in {
      GenericLambda.findElement(Map("lala"->1),List("lala")) shouldEqual 1
    }
    "should fail findElement in first level if it doesn't exist" in {
      intercept[Exception]{GenericLambda.findElement(Map("lala"->1),List("lala2"))}
    }
    "should findElement embedded"in{
      GenericLambda.findElement(Map("lala"->Map("lala2"->1)),List("lala","lala2")) shouldEqual 1
    }

    "should fail findElement embedded if it doesn't exist"in{
      intercept[Exception]{GenericLambda.findElement(Map("lala"->Map("lala2"->1)),List("lala","lala3"))}
    }

    "should fail findElement if empty path"in{
      intercept[Exception]{GenericLambda.findElement(Map("lala"->Map("lala2"->1)),List())}
    }

    //changeElement
    "should changeElement in first level" in {
      GenericLambda.changeElement(Map("lala"->1),List("lala"),2) shouldEqual Map("lala"->2)
    }
    "should fail changeElement in first level if it doesn't exist" in {
      intercept[Exception]{GenericLambda.changeElement(Map("lala"->1),List("lala2"),2)}
    }
    "should changeElement embedded"in{
      GenericLambda.changeElement(Map("lala"->Map("lala2"->1)),List("lala","lala2"),2) shouldEqual Map("lala"->Map("lala2"->2))
    }

    "should fail changeElement embedded if it doesn't exist"in{
      intercept[Exception]{GenericLambda.changeElement(Map("lala"->Map("lala2"->1)),List("lala","lala3"),2)}
    }

    "should fail changeElement if empty path"in{
      intercept[Exception]{GenericLambda.changeElement(Map("lala"->Map("lala2"->1)),List(),2)}
    }

    //addElement
    "should addElement to map in first level" in {
      GenericLambda.addElement(Map("lala"->Map("lala2"->1)),Map("lala"->Map("lala2"->1)),List("lala"),List("lala"),Map("lala3"->2)) shouldEqual Map("lala" -> Map("lala3" -> 2, "lala2" -> 1))
    }
    "should fail addElement to map in first level if it doesn't exist" in {
      intercept[Exception]{GenericLambda.addElement(Map("lala"->1),Map("lala"->1),List("lala2"),List("lala2"),2)}
    }
    "should addElement to map embedded"in{
      GenericLambda.addElement(Map("lala"->Map("lala2"->Map("lala3"->3))),Map("lala"->Map("lala2"->Map("lala3"->3))),List("lala","lala2"),List("lala","lala2"),Map("lala4"->4)) shouldEqual Map("lala" -> Map("lala2" -> Map("lala4" -> 4, "lala3" -> 3)))
    }

    "should fail addElement to map embedded if it doesn't exist"in{
      intercept[Exception]{GenericLambda.addElement(Map("lala"->Map("lala2"->1)),Map("lala"->Map("lala2"->1)),List("lala","lala3"),List("lala","lala3"),2)}
    }

    "should fail addElement to map if empty path"in{
      intercept[Exception]{GenericLambda.addElement(Map("lala"->Map("lala2"->1)),Map("lala"->Map("lala2"->1)),List(),List(),2)}
    }

    "should addElement to list in first level" in {
      GenericLambda.addElement(Map("lala"->List(1)),Map("lala"->List(1)),List("lala"),List("lala"),2) shouldEqual Map("lala"->List(2,1))
    }
    "should fail addElement to list in first level if it doesn't exist" in {
      intercept[Exception]{GenericLambda.addElement(Map("lala"->List(1)),Map("lala"->List(1)),List("lala2"),List("lala2"),2)}
    }
    "should addElement to list embedded"in{
      GenericLambda.addElement(Map("lala"->Map("lala2"->List(1))),Map("lala"->Map("lala2"->List(1))),List("lala","lala2"),List("lala","lala2"),2) shouldEqual Map("lala"->Map("lala2"->List(2,1)))
    }

    "should fail addElement to list embedded if it doesn't exist"in{
      intercept[Exception]{GenericLambda.addElement(Map("lala"->Map("lala2"->List(1))),Map("lala"->Map("lala2"->List(1))),List("lala","lala3"),List("lala","lala3"),2)}
    }

    "should fail addElement to list if empty path"in{
      intercept[Exception]{GenericLambda.addElement(Map("lala"->Map("lala2"->List(1))),Map("lala"->Map("lala2"->List(1))),List(),List(),2)}
    }

    //removeElement
    "should removeElement in map in first level" in {
      GenericLambda.removeElement(Map("lala"->Map("lala2"->1,"lala3" -> 2)),Map("lala"->Map("lala2"->1,"lala3" -> 2)),List("lala"),List("lala"),Map("lala3"->2)) shouldEqual Map("lala" -> Map("lala2" -> 1))
    }
    "should fail removeElement in map in first level if it doesn't exist" in {
      intercept[Exception]{GenericLambda.removeElement(Map("lala"->1),Map("lala"->1),List("lala2"),List("lala2"),2)}
    }
    "should removeElement in map embedded"in{
      GenericLambda.removeElement(Map("lala"->Map("lala2"->Map("lala4" -> 4, "lala3" -> 3))),Map("lala"->Map("lala2"->Map("lala4" -> 4, "lala3" -> 3))),List("lala","lala2"),List("lala","lala2"),Map("lala4"->4)) shouldEqual Map("lala" -> Map("lala2" -> Map("lala3" -> 3)))
    }

    "should fail removeElement in map embedded if it doesn't exist"in{
      intercept[Exception]{GenericLambda.removeElement(Map("lala"->Map("lala2"->1)),Map("lala"->Map("lala2"->1)),List("lala","lala3"),List("lala","lala3"),2)}
    }

    "should fail removeElement in map if empty path"in{
      intercept[Exception]{GenericLambda.removeElement(Map("lala"->Map("lala2"->1)),Map("lala"->Map("lala2"->1)),List(),List(),2)}
    }

    "should removeElement in list in first level" in {
      GenericLambda.removeElement(Map("lala"->List(1,2)),Map("lala"->List(1,2)),List("lala"),List("lala"),2) shouldEqual Map("lala"->List(1))
    }
    "should fail removeElement in list in first level if it doesn't exist" in {
      intercept[Exception]{GenericLambda.removeElement(Map("lala"->List(1)),Map("lala"->List(1)),List("lala2"),List("lala2"),2)}
    }
    "should removeElement in list embedded"in{
      GenericLambda.removeElement(Map("lala"->Map("lala2"->List(1,2))),Map("lala"->Map("lala2"->List(1,2))),List("lala","lala2"),List("lala","lala2"),2) shouldEqual Map("lala"->Map("lala2"->List(1)))
    }

    "should fail removeElement in list embedded if it doesn't exist"in{
      intercept[Exception]{GenericLambda.removeElement(Map("lala"->Map("lala2"->List(1))),Map("lala"->Map("lala2"->List(1))),List("lala","lala3"),List("lala","lala3"),2)}
    }

    "should fail removeElement in list if empty path"in{
      intercept[Exception]{GenericLambda.removeElement(Map("lala"->Map("lala2"->List(1))),Map("lala"->Map("lala2"->List(1))),List(),List(),2)}
    }

    "should removeElement from position in list in first level" in {
      GenericLambda.removeElementFromPosition(Map("lala"->List(1,2,3)),Map("lala"->List(1,2,3)),List("lala"),List("lala"),1) shouldEqual Map("lala"->List(1,3))
    }
    "should removeElement from positionin list embedded"in{
      GenericLambda.removeElementFromPosition(Map("lala"->Map("lala2"->List(1,2,3))),Map("lala"->Map("lala2"->List(1,2,3))),List("lala","lala2"),List("lala","lala2"),1) shouldEqual Map("lala"->Map("lala2"->List(1,3)))
    }


    "should getElement from position in list in first level" in {
      GenericLambda.getElementFromPosition(Map("lala"->List(1,2,3)),Map("lala"->List(1,2,3)),List("lala"),List("lala"),1) shouldEqual 2
    }
    "should getElement from positionin list embedded"in{
      GenericLambda.getElementFromPosition(Map("lala"->Map("lala2"->List(1,2,3))),Map("lala"->Map("lala2"->List(1,2,3))),List("lala","lala2"),List("lala","lala2"),1) shouldEqual 2
    }

    "should addElement to position in list in first level" in {
      GenericLambda.addElementToPosition(Map("lala"->List(1,3)),Map("lala"->List(1,3)),List("lala"),List("lala"),2,1) shouldEqual Map("lala"->List(1,2,3))
    }
    "should addElement to positionin list embedded"in{
      GenericLambda.addElementToPosition(Map("lala"->Map("lala2"->List(1,3))),Map("lala"->Map("lala2"->List(1,3))),List("lala","lala2"),List("lala","lala2"),2,1) shouldEqual Map("lala"->Map("lala2"->List(1,2,3)))
    }

    "should changeElement in position in list in first level" in {
      GenericLambda.changeElementInPosition(Map("lala"->List(1,2,3)),Map("lala"->List(1,2,3)),List("lala"),List("lala"),4,1) shouldEqual Map("lala"->List(1,4,3))
    }
    "should changeElement in positionin list embedded"in{
      GenericLambda.changeElementInPosition(Map("lala"->Map("lala2"->List(1,2,3))),Map("lala"->Map("lala2"->List(1,2,3))),List("lala","lala2"),List("lala","lala2"),4,1) shouldEqual Map("lala"->Map("lala2"->List(1,4,3)))
    }

  }
}
