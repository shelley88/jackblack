package engine.rules

import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.testkit.TestKit
import akka.util.Timeout
import org.scalatest.{Matchers, WordSpecLike}
import util.MockModelActor

import scala.concurrent.Await
import scala.concurrent.duration._

class FunctionActorSpec extends TestKit(ActorSystem("AkkaTestSystem")) with WordSpecLike with Matchers{
  implicit val timeout: Timeout = Timeout(15 seconds)
  val modelActor = system.actorOf(Props(new MockModelActor(Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2))))),"Model")
  val model = Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
  val functionActor=system.actorOf(Props(new FunctionActor(modelActor)),"functionActor")

  "An operation actor" must {
    "should return Exception" in {
      val wait=functionActor ? "exception"
      val ret=Await.result(wait,1000 millis)
      ret.getClass shouldEqual (new Exception).getClass
    }
    "should return values" in {
      val wait=functionActor ? (Map("type"->"value", "value"->7),Map[String,Any](),Map("value"->Map("type"->"value", "value"->7)),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 7
    }

    //    "should fail for not known operations" in {
    //      intercept[Error]{functionActor ? ("unknown",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4))}
    ////      val ret=Await.result(wait,1000 millis)
    ////      ret.getClass shouldEqual (new Exception).getClass
    //    }
    "should return paths that exist" in{
      val wait=functionActor ? (Map("type"->"path", "path"->"hand1"),Map[String,Any](),Map("value"->Map("type"->"value", "value"->7)),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 1
    }

    //    "should not Add if paths don't exist" in{
    //      val wait=functionActor ? ("Add",List(Map("type"->"path", "path"->"unknown"),Map("type"->"path", "path"->"hand1")),Map("lolo"->4))
    //      val ret=Await.result(wait,1000 millis)
    //      ret shouldEqual 2
    //    }

    "should Add operations that exist" in{
      val wait=functionActor ? (Map("type"->"operation", "operator"->"Add","parameters"->List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7))),Map[String,Any](),Map("value"->Map("type"->"value", "value"->7)),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 14
    }

    "should return input that exist" in{
      val wait=functionActor ? (Map("type"->"input", "input"->"in"),Map[String,Any]("in"->1),Map("value"->Map("type"->"value", "value"->7)),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 1
    }

    "should return functions that exist" in{
      val wait=functionActor ? (Map("type"->"function", "function"->"lala","input"->Map[String,Any]()),Map[String,Any]("in"->1),Map("lala"->Map("type"->"value", "value"->7)),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 7
    }
    "should return subvalue that exist" in{
      val wait=functionActor ? (Map("type"->"subValue", "subValue"->Map("type"->"function","function"->"lala","input"->Map[String,Any]()),"path"->"hand1"),Map[String,Any]("in"->1),Map("lala"->Map("type"->"value", "value"->Map("hand1"->1))),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 1
    }
    "should return indexvalue that exist" in{
      val wait=functionActor ? (Map("type"->"indexValue", "indexValue"->Map("type"->"function","function"->"lala","input"->Map[String,Any]()),"path"->"hand2","index"->1),Map[String,Any]("in"->1),Map("lala"->Map("type"->"value", "value"->Map("hand2"->List(Map("val1" -> 3, "val2" -> 4),Map("val1" -> 1, "val2" -> 2))))),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual Map("val1" -> 1, "val2" -> 2)
    }

  }

}
