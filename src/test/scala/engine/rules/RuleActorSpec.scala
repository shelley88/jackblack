package engine.rules

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import akka.util.Timeout
import org.scalatest.{Matchers, WordSpecLike}
import util.MockModelActor

import scala.concurrent.duration._
import akka.pattern.ask

class RuleActorSpec extends TestKit(ActorSystem("AkkaTestSystem")) with WordSpecLike with Matchers {
  implicit val timeout: Timeout = Timeout(15 seconds)

  val modelActor = system.actorOf(Props(new MockModelActor(Map("hand1" -> "lalal", "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2))))),"Model")
  val ruleActor=system.actorOf(Props(new RuleActor(modelActor)),"pathActor")



}
