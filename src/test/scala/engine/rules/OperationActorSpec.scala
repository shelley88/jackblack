package engine.rules

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import org.scalatest.{Matchers, WordSpecLike}
import akka.pattern.ask
import akka.util.Timeout
import util.MockModelActor

import scala.concurrent.Await
import scala.concurrent.duration._

class OperationActorSpec extends TestKit(ActorSystem("AkkaTestSystem")) with WordSpecLike with Matchers{
  implicit val timeout: Timeout = Timeout(15 seconds)
  val modelActor = system.actorOf(Props(new MockModelActor(Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2))))),"Model")
  val model =Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
  val operationActor=system.actorOf(Props(new OperationActor(modelActor)),"operationActor")

  "An operation actor" must {
    "should return Exception" in {
      val wait=operationActor ? "exception"
      val ret=Await.result(wait,1000 millis)
      ret.getClass shouldEqual (new Exception).getClass
    }
    "should Add values" in {
      val wait=operationActor ? ("Add",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 14
    }
    "should Subtract values" in {
      val wait=operationActor ? ("Subtract",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 0
    }
    "should Multiply values" in {
      val wait=operationActor ? ("Multiply",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 49
    }
    "should Divide values" in {
      val wait=operationActor ? ("Divide",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 1
    }
    "should GreaterThan values" in {
      val wait=operationActor ? ("GreaterThan",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual false
    }
    "should GreaterOrEquals values" in {
      val wait=operationActor ? ("GreaterOrEquals",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual true
    }
    "should LessThan values" in {
      val wait=operationActor ? ("LessThan",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual false
    }
    "should LessOrEquals values" in {
      val wait=operationActor ? ("LessOrEquals",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual true
    }
    "should Equals values" in {
      val wait=operationActor ? ("Equals",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual true
    }
    "should Different values" in {
      val wait=operationActor ? ("Different",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual false
    }
    "should Shuffle values" in {
      val wait=operationActor ? ("Shuffle",List(Map("type"->"value", "value"->List(7,7))),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual List(7,7)
    }
    "should GetFirst values" in {
      val wait=operationActor ? ("GetFirst",List(Map("type"->"value", "value"->List(7,1))),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 7
    }
//    "should fail for not known operations" in {
//      intercept[Error]{operationActor ? ("unknown",List(Map("type"->"value", "value"->7),Map("type"->"value", "value"->7)),Map("lolo"->4))}
////      val ret=Await.result(wait,1000 millis)
////      ret.getClass shouldEqual (new Exception).getClass
//    }
    "should Add paths that exist" in{
      val wait=operationActor ? ("Add",List(Map("type"->"path", "path"->"hand1"),Map("type"->"path", "path"->"hand1")),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 2
    }

//    "should not Add if paths don't exist" in{
//      val wait=operationActor ? ("Add",List(Map("type"->"path", "path"->"unknown"),Map("type"->"path", "path"->"hand1")),Map("lolo"->4))
//      val ret=Await.result(wait,1000 millis)
//      ret shouldEqual 2
//    }

    "should Add operations that exist" in{
      val wait=operationActor ? ("Add",List(Map("type"->"operation", "operator"->"Add","parameters"->List(Map("type"->"path", "path"->"hand1"),Map("type"->"path", "path"->"hand1"))),Map("type"->"path", "path"->"hand1")),Map("lolo"->4),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 3
    }

    "should Add functions that exist" in{
      val wait=operationActor ? ("Add",List(Map("type"->"function", "function"->"lala","input"->Map[String,Any]()),Map("type"->"value", "value"->1)),Map("lala"->Map("type"->"value", "value"->7)),model)
      val ret=Await.result(wait,1000 millis)
      ret shouldEqual 8
    }


    //must add errors

  }

}
