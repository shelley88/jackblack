package engine.rules

import akka.pattern.ask
import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import akka.util.Timeout
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.Await
import scala.concurrent.duration._

class ModelActorSpec extends TestKit(ActorSystem("AkkaTestSystem")) with WordSpecLike with Matchers{
  implicit val timeout: Timeout = Timeout(15 seconds)
  val mapper = new ObjectMapper
  mapper.registerModule(DefaultScalaModule)

  val modelActor=system.actorOf(Props(new ModelActor()),"modelActor")
  val inputJSON = "{\"hand1\":1,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}]}"
  val model: Map[String, Any] = mapper.readValue(inputJSON, classOf[Map[String, Any]])
  val modifyModel= mapper.readValue("{\"hand1\":7,\n\"hand2\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}],\n\"table\":[{\"val1\":1,\"val2\":2},{\"val1\":1,\"val2\":2}]}", classOf[Map[String, Any]])

  "A ModelActor" must{
    "should hold model information" in{
      modelActor ! model
      val wait =modelActor ? "getModel"
      val result = Await.result(wait,1000 millis)

      result shouldEqual Map("hand1" -> 1, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }
    "should allow modifying" in {
        val wait = modelActor ? modifyModel
        val result = Await.result(wait, 1000 millis)

        result shouldEqual Map("hand1" -> 7, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }
    "should return model" in {
      val wait = modelActor ? "getModel"
      val result = Await.result(wait, 1000 millis)

      result shouldEqual Map("hand1" -> 7, "hand2" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)), "table" -> List(Map("val1" -> 1, "val2" -> 2), Map("val1" -> 1, "val2" -> 2)))
    }

    "should fail properly initializing" in{
      val modelActorNew=system.actorOf(Props(new ModelActor()),"modelActor2")
      val wait = modelActorNew ? 7
      val result = Await.result(wait, 1000 millis)
      result shouldEqual "modelActor not matching 7"
    }

    "should fail properly" in{
      val wait = modelActor ? 7
      val result = Await.result(wait, 1000 millis)
      result shouldEqual "modelActor not matching 7"
    }
  }
}
