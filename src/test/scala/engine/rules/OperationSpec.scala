package engine.rules

import org.scalatest.{Matchers, WordSpec}

class OperationSpec extends WordSpec with Matchers{

  "An operation" must {
    //Equals
    "should be Equals for Strings" in {
      Operations.Equals(List("hola", "hola","hola","hola")) shouldEqual true
    }
    "should be Equals for Strings if the list is length 1" in {
      Operations.Equals(List("hola")) shouldEqual true
    }
    "should be Equals if the list is empty" in {
      Operations.Equals(List()) shouldEqual true
    }
    "should not be Equals for Strings" in {
      Operations.Equals(List("hola", "hola2","hola","hola")) shouldEqual false
    }

    "should be Equals for Integer" in {
      Operations.Equals(List(1, 1,1,1.0)) shouldEqual true
    }
    "should be Equals for Integer if the list is length 1" in {
      Operations.Equals(List(1)) shouldEqual true
    }

    "should not be Equals for Integer" in {
      Operations.Equals(List(1,2,3,4)) shouldEqual false
    }

    "should be Equals for Map" in {
      Operations.Equals(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7))) shouldEqual true
    }
    "should be Equals for Map if the list is length 1" in {
      Operations.Equals(List(Map[String,Any]("lala"->7))) shouldEqual true
    }

    "should not be Equals for Map" in {
      Operations.Equals(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->8),Map[String,Any]("lala"->8))) shouldEqual false
    }

    "should not be Equals for mixed list" in {
      Operations.Equals(List("a",1)) shouldEqual false
    }

    //Different
    "should not be Different for Strings" in {
      Operations.Different(List("hola", "hola","hola","hola")) shouldEqual false
    }
    "should not be Different for Strings with some equals" in {
      Operations.Different(List("hola", "hola","hola2","hola3")) shouldEqual false
    }
    "should be Different for Strings if the list is length 1" in {
      Operations.Different(List("hola")) shouldEqual true
    }
    "should be Different if the list is empty" in {
      Operations.Different(List()) shouldEqual true
    }
    "should be Different for Strings" in {
      Operations.Different(List("hola", "hola2","hola3","hola4")) shouldEqual true
    }

    "should not be Different for Integer" in {
      Operations.Different(List(1, 1,1,1.0)) shouldEqual false
    }
    "should not be Different for Integer with some equals" in {
      Operations.Different(List(1, 1,2,3)) shouldEqual false
    }
    "should be Different for Integer if the list is length 1" in {
      Operations.Different(List(1)) shouldEqual true
    }

    "should be Different for Integer" in {
      Operations.Different(List(1,2,3,4)) shouldEqual true
    }

    "should not be Different for Map" in {
      Operations.Different(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7))) shouldEqual false
    }
    "should not be Different for Map with some equals" in {
      Operations.Different(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->8),Map[String,Any]("lala"->9),Map[String,Any]("lala"->7))) shouldEqual false
    }
    "should be Different for Map if the list is length 1" in {
      Operations.Different(List(Map[String,Any]("lala"->7))) shouldEqual true
    }

    "should be Different for Map" in {
      Operations.Different(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->8),Map[String,Any]("lala"->9))) shouldEqual true
    }

    "should be Different for mixed list" in {
      Operations.Different(List("a",1)) shouldEqual true
    }

    "should not be Different for mixed list" in {
      Operations.Different(List("a",1,1)) shouldEqual false
    }

    // Contains

    // ContainsKey
    "Should find the first key in a 2 keys object" in {
      Operations.ContainsKey(List("a", Map[String, Any]("a" -> 1, "b" -> 2))) shouldEqual true
    }

    "Should find the second key in a 2 keys object" in {
      Operations.ContainsKey(List("b", Map[String, Any]("a" -> 1, "b" -> 2))) shouldEqual true
    }

    "Should not find a key that is not in a 2 keys object" in {
      Operations.ContainsKey(List("c", Map[String, Any]("a" -> 1, "b" -> 2))) shouldEqual false
    }

    //NotEquals
    "should be NotEquals for Strings" in {
      Operations.NotEquals(List("hola", "hola","hola","hola")) shouldEqual false
    }
    "should be NotEquals for Strings with some equals" in {
      Operations.NotEquals(List("hola", "hola","hola2","hola3")) shouldEqual true
    }
    "should not be NotEquals for Strings if the list is length 1" in {
      Operations.NotEquals(List("hola")) shouldEqual false
    }
    "should not be NotEquals if the list is empty" in {
      Operations.NotEquals(List()) shouldEqual false
    }
    "should not be NotEquals for Integer" in {
      Operations.NotEquals(List(1, 1,1,1.0)) shouldEqual false
    }
    "should be NotEquals for Integer with some equals" in {
      Operations.NotEquals(List(1, 1,2,3)) shouldEqual true
    }
    "should not be NotEquals for Integer if the list is length 1" in {
      Operations.NotEquals(List(1)) shouldEqual false
    }

    "should be NotEquals for Integer" in {
      Operations.NotEquals(List(1,2,3,4)) shouldEqual true
    }

    "should not be NotEquals for Map" in {
      Operations.NotEquals(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->7))) shouldEqual false
    }
    "should be NotEquals for Map with some equals" in {
      Operations.NotEquals(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->7),Map[String,Any]("lala"->8),Map[String,Any]("lala"->9),Map[String,Any]("lala"->7))) shouldEqual true
    }
    "should not be NotEquals for Map if the list is length 1" in {
      Operations.NotEquals(List(Map[String,Any]("lala"->7))) shouldEqual false
    }

    "should be NotEquals for Map" in {
      Operations.NotEquals(List(Map[String,Any]("lala"->7),Map[String,Any]("lala"->8),Map[String,Any]("lala"->9))) shouldEqual true
    }

    "should be NotEquals for mixed list" in {
      Operations.NotEquals(List("a",1)) shouldEqual true
    }

    //GreaterThan
    "should be GreaterThan for String in List of size 2" in{
      Operations.GreaterThan(List("b","a")) shouldEqual true
    }
    "should be GreaterThan for Int in List of size 2" in{
      Operations.GreaterThan(List(2,1)) shouldEqual true
    }

    "should be GreaterThan for other throws exception" in{
      Operations.GreaterThan(List(Map[String,Any]())) shouldEqual false
    }

    "should be GreaterThan for String in List of size greater than 2" in{
      Operations.GreaterThan(List("d","c","b","a")) shouldEqual true
    }
    "should be GreaterThan for Int in List of size greater than 2" in{
      Operations.GreaterThan(List(4,3,2,1)) shouldEqual true
    }

    "should not be GreaterThan for String in List of size 2" in{
      Operations.GreaterThan(List("a","b")) shouldEqual false
    }
    "should not be GreaterThan for Int in List of size 2" in{
      Operations.GreaterThan(List(1,2)) shouldEqual false
    }

    "should not be GreaterThan for String in List of equals size 2" in{
      Operations.GreaterThan(List("a","a")) shouldEqual false
    }
    "should not be GreaterThan for Int in List of equals size 2" in{
      Operations.GreaterThan(List(1,1)) shouldEqual false
    }

    "should not be GreaterThan for String in List of size greater than 2" in{
      Operations.GreaterThan(List("d","c","a","b")) shouldEqual false
    }
    "should not be GreaterThan for Int in List of size greater than 2" in{
      Operations.GreaterThan(List(4,3,1,2)) shouldEqual false
    }
    "should should throw exception for GreaterThan on a list smaller than 2" in{
      Operations.GreaterThan(List(1)) shouldEqual false
    }

    "should not be GreaterThan for String in List of equals size greater than 2" in{
      Operations.GreaterThan(List("d","c","a","a")) shouldEqual false
    }
    "should not be GreaterThan for Int in List of equals size greater than 2" in{
      Operations.GreaterThan(List(4,3,1,1)) shouldEqual false
    }

    "should fail GreaterThan for mixed list" in {
      Operations.GreaterThan(List("a",1)) shouldEqual false
    }

    //GreaterOrEquals
    "should be GreaterOrEquals for String in List of size 2" in{
      Operations.GreaterOrEquals(List("b","a")) shouldEqual true
    }
    "should be GreaterOrEquals for Int in List of size 2" in{
      Operations.GreaterOrEquals(List(2,1)) shouldEqual true
    }

    "should be GreaterOrEquals for other throws exception" in{
      Operations.GreaterOrEquals(List(Map[String,Any]())) shouldEqual false
    }

    "should be GreaterOrEquals for String in List of size greater than 2" in{
      Operations.GreaterOrEquals(List("d","c","b","a")) shouldEqual true
    }
    "should be GreaterOrEquals for Int in List of size greater than 2" in{
      Operations.GreaterOrEquals(List(4,3,2,1)) shouldEqual true
    }

    "should not be GreaterOrEquals for String in List of size 2" in{
      Operations.GreaterOrEquals(List("a","b")) shouldEqual false
    }
    "should not be GreaterOrEquals for Int in List of size 2" in{
      Operations.GreaterOrEquals(List(1,2)) shouldEqual false
    }

    "should be GreaterOrEquals for String in List of equals size 2" in{
      Operations.GreaterOrEquals(List("a","a")) shouldEqual true
    }
    "should be GreaterOrEquals for Int in List of equals size 2" in{
      Operations.GreaterOrEquals(List(1,1)) shouldEqual true
    }

    "should not be GreaterOrEquals for String in List of size greater than 2" in{
      Operations.GreaterOrEquals(List("d","c","a","b")) shouldEqual false
    }
    "should not be GreaterOrEquals for Int in List of size greater than 2" in{
      Operations.GreaterOrEquals(List(4,3,1,2)) shouldEqual false
    }
    "should throw exception for GreaterOrEquals on a list smaller than 2" in{
      Operations.GreaterOrEquals(List(1)) shouldEqual false
    }

    "should be GreaterOrEquals for String in List of equals size greater than 2" in{
      Operations.GreaterOrEquals(List("d","c","a","a")) shouldEqual true
    }
    "should be GreaterOrEquals for Int in List of equals size greater than 2" in{
      Operations.GreaterOrEquals(List(4,3,1,1)) shouldEqual true
    }

    "should fail GreaterOrEquals for mixed list" in {
      Operations.GreaterOrEquals(List("a",1)) shouldEqual false
    }

    //LessThan
    "should be LessThan for String in List of size 2" in{
      Operations.LessThan(List("a","b")) shouldEqual true
    }
    "should be LessThan for Int in List of size 2" in{
      Operations.LessThan(List(1,2)) shouldEqual true
    }

    "should be LessThan for other throws exception" in{
      Operations.LessThan(List(Map[String,Any]())) shouldEqual false
    }

    "should be LessThan for String in List of size greater than 2" in{
      Operations.LessThan(List("a","b","c","d")) shouldEqual true
    }
    "should be LessThan for Int in List of size greater than 2" in{
      Operations.LessThan(List(1,2,3,4)) shouldEqual true
    }

    "should not be LessThan for String in List of size 2" in{
      Operations.LessThan(List("b","a")) shouldEqual false
    }
    "should not be LessThan for Int in List of size 2" in{
      Operations.LessThan(List(2,1)) shouldEqual false
    }

    "should not be LessThan for String in List of equals size 2" in{
      Operations.LessThan(List("a","a")) shouldEqual false
    }
    "should not be LessThan for Int in List of equals size 2" in{
      Operations.LessThan(List(1,1)) shouldEqual false
    }

    "should not be LessThan for String in List of size greater than 2" in{
      Operations.LessThan(List("d","c","a","b")) shouldEqual false
    }
    "should not be LessThan for Int in List of size greater than 2" in{
      Operations.LessThan(List(4,3,1,2)) shouldEqual false
    }
    "should throw exception for LessThan on a list smaller than 2" in{
      Operations.LessThan(List(1)) shouldEqual false
    }

    "should not be LessThan for String in List of equals size greater than 2" in{
      Operations.LessThan(List("a","a","c","d")) shouldEqual false
    }
    "should be LessThan for Int in List of equals size greater than 2" in{
      Operations.LessThan(List(1,1,3,4)) shouldEqual false
    }

    "should fail LessThan for mixed list" in {
      Operations.LessThan(List("a",1)) shouldEqual false
    }

    //LessOrEquals
    "should be LessOrEquals for String in List of size 2" in{
      Operations.LessOrEquals(List("a","b")) shouldEqual true
    }
    "should be LessOrEquals for Int in List of size 2" in{
      Operations.LessOrEquals(List(1,2)) shouldEqual true
    }

    "should be LessOrEquals for other throws exception" in{
      Operations.LessOrEquals(List(Map[String,Any]())) shouldEqual false
    }

    "should be LessOrEquals for String in List of size greater than 2" in{
      Operations.LessOrEquals(List("a","b","c","d")) shouldEqual true
    }
    "should be LessOrEquals for Int in List of size greater than 2" in{
      Operations.LessOrEquals(List(1,2,3,4)) shouldEqual true
    }

    "should not be LessOrEquals for String in List of size 2" in{
      Operations.LessOrEquals(List("b","a")) shouldEqual false
    }
    "should not be LessOrEquals for Int in List of size 2" in{
      Operations.LessOrEquals(List(2,1)) shouldEqual false
    }

    "should be LessOrEquals for String in List of equals size 2" in{
      Operations.LessOrEquals(List("a","a")) shouldEqual true
    }
    "should be LessThan for Int in List of equals size 2" in{
      Operations.LessOrEquals(List(1,1)) shouldEqual true
    }

    "should not be LessOrEquals for String in List of size greater than 2" in{
      Operations.LessOrEquals(List("d","c","a","b")) shouldEqual false
    }
    "should not be LessOrEquals for Int in List of size greater than 2" in{
      Operations.LessOrEquals(List(4,3,1,2)) shouldEqual false
    }
    "should throw exception for LessOrEquals on a list smaller than 2" in{
       Operations.LessOrEquals(List(1)) shouldEqual false
    }

    "should not be LessOrEquals for String in List of equals size greater than 2" in{
      Operations.LessOrEquals(List("a","a","c","d")) shouldEqual true
    }
    "should be LessOrEquals for Int in List of equals size greater than 2" in{
      Operations.LessOrEquals(List(1,1,3,4)) shouldEqual true
    }

    "should fail LessOrEquals for mixed list" in {
      Operations.LessOrEquals(List("a",1)) shouldEqual false
    }

    //Add
    "should Add a List of Int" in {
      Operations.Add(List(1,2,3)) shouldEqual 6
    }
    "should Add a List of Int size 1" in {
      Operations.Add(List(1)) shouldEqual 1
    }

    "should Add a List of String" in {
      Operations.Add(List("a","b","c")) shouldEqual "abc"
    }

    "should Add a List of String size 1" in {
      Operations.Add(List("a")) shouldEqual "a"
    }

    "should fail Add an empty List" in {
      Operations.Add(List.empty) shouldBe an [Exception]
    }

    "should fail Add for mixed list" in {
     Operations.Add(List("a",1)) shouldBe an [Exception]
    }

    "should fail Add for map list" in {
      Operations.Add(List(Map[String,Any](),Map[String,Any]())) shouldBe an [Exception]
    }

    //Subtract
    "should Subtract a List of Int" in {
      Operations.Subtract(List(6,2,1)) shouldEqual 3
    }
    "should Subtract a List of Int size 2" in {
      Operations.Subtract(List(2,1)) shouldEqual 1
    }

    "should Subtract a List of String" in {
      Operations.Subtract(List("abc","b","c")) shouldEqual "a"
    }

    "should Subtract a List of String size 2" in {
      Operations.Subtract(List("ab","b")) shouldEqual "a"
    }

    "should fail Subtract a List of String size 1" in {
      intercept[Exception] {Operations.Subtract(List("ab"))}
    }

    "should fail Subtract a List of Int size 1" in {
      intercept[Exception] {Operations.Subtract(List(1))}
    }

    "should fail Subtract an empty List" in {
      intercept[Exception] {Operations.Subtract(List())}
    }

    "should fail Subtract for mixed list" in {
      intercept[Exception] {Operations.Subtract(List("a",1))}
    }

    "should fail Subtract for map list" in {
      intercept[Exception] {Operations.Subtract(List(Map[String,Any](),Map[String,Any]()))}
    }

    //Multiply
    "should Multiply a List of Int" in {
      Operations.Multiply(List(6,2,1)) shouldEqual 12
    }
    "should Multiply a List of Int size 1" in {
      Operations.Multiply(List(2)) shouldEqual 2
    }

    "should fail Multiply an empty List" in {
      intercept[Exception] {Operations.Multiply(List())}
    }

    "should fail Multiply for mixed list" in {
      intercept[Exception] {Operations.Multiply(List("a",1))}
    }

    "should fail Multiply for map list" in {
      intercept[Exception] {Operations.Multiply(List(Map[String,Any](),Map[String,Any]()))}
    }

    //Divide
    "should Divide a List of Int" in {
      Operations.Divide(List(6,2,1)) shouldEqual 3
    }
    "should Divide a List of Int size 1" in {
      Operations.Divide(List(2,2)) shouldEqual 1
    }

    "should fail Divide List of int size 1" in {
      intercept[Exception] {Operations.Divide(List(1))}
    }

    "should fail Divide an empty List" in {
      intercept[Exception] {Operations.Divide(List())}
    }

    "should fail Divide for mixed list" in {
      intercept[Exception] {Operations.Divide(List("a",1))}
    }

    "should fail Divide for map list" in {
      intercept[Exception] {Operations.Divide(List(Map[String,Any](),Map[String,Any]()))}
    }

    //GetFirst
    "should get first map in list" in {
      Operations.GetFirst(List(Map[String,Any]("lala"->1),Map[String,Any]("lolo"->2))) shouldEqual Map[String,Any]("lala"->1)
    }

    "should get first map in list of list" in {
      Operations.GetFirst(List(List(Map[String,Any]("lala"->1),Map[String,Any]("lolo"->2)))) shouldEqual Map[String,Any]("lala"->1)
    }

    //GetPositionInList
    "should GetPositionInList  map in list" in {
      Operations.GetPositionInList(List("name","shelley",List(Map[String,Any]("name"->"shelley"),Map[String,Any]("name"->"ricardo")))) shouldEqual 0
    }

    //Shuffle
    "should shuffle List" in {
      Operations.Shuffle(List(7,6,5,4,3,2,1)).forall(p => List(7,6,5,4,3,2,1).contains(p)) shouldEqual true
      Operations.Shuffle(List(7,6,5,4,3,2,1)) == List(7,6,5,4,3,2,1) shouldEqual false
    }

    "should shuffle list size 1" in {
      Operations.Shuffle(List(7)).forall(p => List(7).contains(p)) shouldEqual true
      Operations.Shuffle(List(7)) == List(7) shouldEqual true
    }
    "should shuffle empty list" in {
      Operations.Shuffle(List()) shouldEqual List()
    }

    "random should work for integers in order" in {
      (Operations.Random(List(1,13)).asInstanceOf[Int] >= 1) shouldEqual true
      (Operations.Random(List(1,13)).asInstanceOf[Int] <= 13) shouldEqual true
    }

    //Random
    "random should not work for integers not in order" in {
      intercept[Exception] {Operations.Random(List(13,1))}
    }

    "random should not work for more than 2 Integers" in {
      intercept[Exception] {Operations.Random(List(1,2,3))}
    }
    "random should not work for less than 2 Integers" in {
      intercept[Exception] {Operations.Random(List(1))}
    }
    "random should not work for String" in {
      intercept[Exception] {Operations.Random(List("1","13"))}
    }

    "random should work for same integer" in {
      (Operations.Random(List(13,13)).asInstanceOf[Int] == 13) shouldEqual true
    }

    //Lenth
    "length should work for empty list" in {
      (Operations.Length(List())) shouldEqual 0
    }

    "length should work for list of integers" in {
      (Operations.Length(List(1,2,3))) shouldEqual 3
    }

    "length should work for list of strings" in {
      (Operations.Length(List("1","2","3"))) shouldEqual 3
    }

    //Modulo
    "modulo should not work for String" in {
      intercept[Exception] {Operations.Modulo(List("1","13"))}
    }
    "modulo should not work for less than 2 Integers" in {
      intercept[Exception] {Operations.Modulo(List(1))}
    }

    "modulo should not work for more than 2 Integers" in {
      intercept[Exception] {Operations.Modulo(List(1,2,3))}
    }

    "modulo should work for 2 Integers" in {
      Operations.Modulo(List(5,2)) shouldEqual 1
    }

  }

}
