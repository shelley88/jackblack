package engine.rules

import akka.pattern.ask
import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout
import errors.ErrorThing.SomeError
import shapeless.TypeCase

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

class OperationActor(modelPath:ActorRef) extends Actor{
  implicit val timeout = Timeout(10 seconds)
  val ListOfMap = TypeCase[List[Map[String,_]]]
  val List = TypeCase[List[(String,_)]]

  val MapOfStringAny = TypeCase[Map[String,_]]

  import context._
  def receive ={
    case (c:String,ListOfMap(a),MapOfStringAny(allFunctions), MapOfStringAny(b)) => {
      val send=sender()
      val z=a.map{ parameter =>

        if (parameter("type") == "value") {
          Future(parameter("value"))
        }
        else if (parameter("type") == "path"){

          Future(GenericLambda.findElement(b, parameter("path").asInstanceOf[String].split('.').toList))
        }
        else if (parameter("type") == "operation"){
          val act=actorOf(Props(new OperationActor(modelPath)))
          val v=act ? (parameter("operator"),parameter("parameters"),allFunctions,b)
          for {
            l <- v
          } yield (context.stop(act))
          v
        }
        else if(parameter("type")=="function"){
          val act=actorOf(Props(new FunctionActor(modelPath)))
          val v=act ? (allFunctions(parameter("function").toString),parameter("input"),allFunctions,b)
          for {
            l <- v
          } yield (context.stop(act))
          v
        }
        else {println("not a valid type in the OperationActor  "+ parameter);throw SomeError("not a valid type in the OperationActor  "+ parameter)}
      }

      val fut =Future.sequence(z)

      val future=fut.onComplete{
        case Success(s) => {
          val op=Operations.getListOperation(c)
          val res=GenericLambda.operateOnList(s,op)
          send ! res
        }
        case Failure(exception) => exception
      }

    }
    case (c:String,ListOfMap(a),MapOfStringAny(allFunctions), MapOfStringAny(b),List(rulesResult)) => {

      val send=sender()
      val z=a.map{ parameter =>

        if (parameter("type") == "value") {
          println("VALUE "+parameter("value"))

          Future(parameter("value"))
        }
        else if (parameter("type") == "path"){
          if(parameter("path").isInstanceOf[String]){
            println("PATH normal "+parameter("path")+" " +GenericLambda.findElement(b, parameter("path").asInstanceOf[String].split('.').toList))
            Future(GenericLambda.findElement(b, parameter("path").asInstanceOf[String].split('.').toList))
          }else if (parameter("path").asInstanceOf[Map[String,Any]]("type")=="input"){
            val in = parameter("path").asInstanceOf[Map[String,Any]]("input")
            val root = parameter("path").asInstanceOf[Map[String,Any]]("root").toString.split('.').toList
            val find = rulesResult.filter(p => p._1 == in).head._2
            println("FIND "+GenericLambda.findElement(b, root ++ find.asInstanceOf[String].split('.').toList))
            println("PATH NEW "+parameter("path").asInstanceOf[Map[String,Any]]("input") +"   "+GenericLambda.findElement(b, root ++ find.asInstanceOf[String].split('.').toList))

            Future(GenericLambda.findElement(b, root ++ find.asInstanceOf[String].split('.').toList))
          }else {println("not a valid path in the OperationActor  "+ parameter);throw SomeError("not a valid type in the OperationActor  "+ parameter)}

        }
        else if (parameter("type") == "operation"){
          val act=actorOf(Props(new OperationActor(modelPath)))

          val v=act ? (parameter("operator"),parameter("parameters"),allFunctions,b,rulesResult)
          for {
            l <- v
          } yield l;//(context.stop(act))

          v
        }
        else if(parameter("type")=="function"){
          val act=actorOf(Props(new FunctionActor(modelPath)))
          val v=act ? (allFunctions(parameter("function").toString),parameter("input"),allFunctions,b)
          for {
            l <- v
          } yield l//(context.stop(act))
          v
        }
        else if (parameter("type") == "rule"){

          val rule=parameter("rule").toString
          val r=rulesResult.filter(p => p._1 == rule)
          println("RULE "+parameter("rule").toString+"    "+r.head._2)
          Future(r.head._2)
        }
        else if (parameter("type") == "input"){
          val rule=parameter("input").toString
          val r=rulesResult.filter(p => p._1 == rule)
          Future(r.head._2)
        }
        else {println("not a valid type in the OperationActor  "+ parameter);throw SomeError("not a valid type in the OperationActor  "+ parameter)}
      }

      val fut =Future.sequence(z)
  println("FUTURE LENTH "+fut+" "+c)

      val future=fut.onComplete{
        case Success(s) => {
          val op=Operations.getListOperation(c)
          val res=GenericLambda.operateOnList(s,op)
          println("FUTURE "+c +" "+res)
          println("OPERATION "+ s+"  "+c)
          send ! res
        }
        case Failure(exception) => println("FUTURE FAILED"+exception);exception
      }

    }

    case b =>println("HAHAHHA ");sender() ! new Exception
  }
}
