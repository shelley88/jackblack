package engine.rules

import akka.actor.Actor
import shapeless.TypeCase

class ModelActor extends Actor{
  val MapOfStringAny = TypeCase[Map[String,_]]
  var data =""
  def receive  ={
    case MapOfStringAny(a) => {
      context.become(hasModel(a))
    }
    case b => println("*****"+b);sender() ! "modelActor not matching "+b
  }

  def hasModel(model:Map[String,_]):Receive={
    case "getModel" => sender() ! model
    case MapOfStringAny(a) =>{
      sender() ! a
      context.become(hasModel(a))
    }
    case b => sender() ! "modelActor not matching "+b
  }
}
