package engine.rules

import akka.pattern.{ask, pipe}

import scala.concurrent.duration._
import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout
import shapeless.TypeCase

import scala.concurrent.Await

class RuleActor(modelPath:ActorRef) extends Actor{
  val MapOfStringAny = TypeCase[Map[String,_]]
  val ListOfMapOfStringAny = TypeCase[List[Map[String,_]]]
  val List = TypeCase[List[(String,_)]]
  import context._
  implicit val timeout = Timeout(15 seconds)
  def receive =setRule

  def setRule:Receive={
    case (MapOfStringAny(input),MapOfStringAny(functions)) => become(processRule(input,functions))
  }

  def processRule(input:Map[String,Any], func:Map[String,Any]):Receive ={
    case( "run", MapOfStringAny(model)) => {
      val s = sender()
      if (input("type") == "value") {
        s ! (self.path.name,input("value"))
      }
      else if (input("type") == "path") {
        s ! (self.path.name,GenericLambda.findElement(model, input("path").asInstanceOf[String].split('.').toList))
      }

      else if (input("type") == "operation") {
        val act=actorOf(Props(new OperationActor(modelPath)))
        val v= act ? (input("operator"),input("parameters"),func,model)
        v.map(p=> (self.path.name,p)).pipeTo(s)

      }
      else if (input("type") == "function") {
        val w=actorOf(Props(new FunctionActor(modelPath)))
        val v=w ? (func(input("function").toString),input("input"),func,model)
        //THIS ONE IS CORRECT AS IT PIPES THE FUTURE
        v.map(p=> (self.path.name,p)).pipeTo(s)

      }
      else{
        println("3  not matching type "+input+"  --  "+func)
      }
    }
    case( "run", MapOfStringAny(model),List(rulesResult)) => {
      println("ruleActor "+ rulesResult + "   "+ input)
      val s = sender()
      val decision = input.get("decision").orElse(Some(false)).get.asInstanceOf[Boolean]
      if (input("type") == "value") {

        s ! (self.path.name,input("value"),decision)
      }
      else if (input("type") == "path") {
        if(input("path").isInstanceOf[String]) {
          println("classic path")
          s ! (self.path.name, GenericLambda.findElement(model, input("path").asInstanceOf[String].split('.').toList), decision)
        }else if (input("path").asInstanceOf[Map[String,Any]]("type")=="input"){
          val in = input("path").asInstanceOf[Map[String,Any]]("input")
          println("new path" + input("path").asInstanceOf[Map[String,Any]]("input") + " "+GenericLambda.findElement(model, in.asInstanceOf[String].split('.').toList))

          s ! (self.path.name, GenericLambda.findElement(model, in.asInstanceOf[String].split('.').toList), decision)
        }
      }

      else if (input("type") == "operation") {
        val act=actorOf(Props(new OperationActor(modelPath)))
        val v= act ? (input("operator"),input("parameters"),func,model,rulesResult)
          v.map(p=> (self.path.name,p,decision)).pipeTo(s)
      }
      else if (input("type") == "function") {
        val w=actorOf(Props(new FunctionActor(modelPath)))
        val v=w ? (func(input("function").toString),input("input"),func,model)
        //THIS ONE IS CORRECT AS IT PIPES THE FUTURE
          v.map(p=> (self.path.name,p,decision)).pipeTo(s)

      }
      else if (input("type") == "rule"){
        val rule=input("rule").toString
        val r=rulesResult.filter(p => p._1 == rule)
        println("RULE!!!   "+self.path.name+"    "+r.head._2)
        s ! (self.path.name,r.head._2,decision)
      }
      else if (input("type") == "input"){
        val rule=input("input").toString
        val r=rulesResult.filter(p => p._1 == rule)
        s ! (self.path.name,r.head._2,decision)
      }
      else if(input("type") == "map"){
        println("****************           ************************")
        val map = input("map").asInstanceOf[List[Map[String,Map[String,Any]]]]
        println("MAP "+map)

        val out=map.map(p => {
          val key =p("key")
          val value =p("value")
          println("KEY "+ key+" VALUE "+value)

          val actKey = actorOf(Props(new RuleActor(modelPath)))
          actKey ! (key, func)
          val resultKey = actKey ? ("run", model, rulesResult)
          val vKey = Await.result(resultKey.mapTo[(String,Any,Boolean)], 1000 millis)
          context.stop(actKey)

          val actValue = actorOf(Props(new RuleActor(modelPath)))
          actValue ! (value, func)
          val resultValue = actValue ? ("run", model, rulesResult)
          val vValue = Await.result(resultValue.mapTo[(String,Any,Boolean)], 1000 millis)
          context.stop(actValue)
          println((vKey -> vValue))
          Map(vKey._2 -> vValue._2)
        })
        s ! (self.path.name,out.flatten.toMap,decision)
      }
      else{
        println("3not matching type "+input+"  --  "+func+" -- "+rulesResult)
      }
    }
    case ("modifyRule",(MapOfStringAny(newInput))) => become(processRule(newInput,func))
    case ("modifyFunctions",(MapOfStringAny(newFunc))) => become(processRule(input,newFunc))
    case b =>println("4not matching type rule "+b)
  }

}
