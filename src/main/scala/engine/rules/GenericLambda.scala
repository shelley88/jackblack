package engine.rules

import shapeless.TypeCase

import scala.util.Try

object GenericLambda {
  val MapOfStringAny = TypeCase[Map[String,_]]
  val ListOfMapOfStringAny = TypeCase[List[Map[String,_]]]
  val ListOfStrings    = TypeCase[List[String]]
  val ListOfAny    = TypeCase[List[_]]
  def operateOnList(list: List[_],op:List[_]=>Any):Any={
   op(list)
  }
  def operateOnListWithPoisition(list: List[_],int: Int,op:(List[_],Int)=>Any):Any={
    op(list,int)
  }

  def findElement(thing: Map[String,_],fieldName:List[String]):Any={
//    println("FIND "+fieldName+" in "+thing)
    if(fieldName.size==1 && thing.nonEmpty){
      try{
        thing(fieldName.head)
      }catch{
        case e:Exception =>  new Exception("No valid field name given for : "+fieldName+" in "+thing)
      }
    }else if(fieldName.size>1){
      findElement(thing(fieldName.head).asInstanceOf[Map[String,Any]],fieldName.tail)
    }else{
       throw new Exception("No valid field name given for : "+fieldName+" in "+thing)
    }
  }

  def changeElement(thing: Map[String,_],fieldName:List[String],newValue:Any): Any ={
    println("Chenge Element")
    if(fieldName.size==1){
      if (thing.contains(fieldName.head)){
        thing + (fieldName.head->newValue)
      }else{
        new Exception("No valid field name given for : "+fieldName)
      }
    }else if(fieldName.size>1){
      thing +(fieldName.head -> changeElement(thing(fieldName.head).asInstanceOf[Map[String,Any]],fieldName.tail,newValue))
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def addElement(thing: Map[String,_],originalThing:Map[String,_],fieldName:List[String],originalFieldName:List[String],newValue:Any): Any ={
    if(fieldName.size==1){
      val v= thing(fieldName.head)
      (v,newValue) match{
        case (ListOfAny(a),b:Any) => {
          changeElement(originalThing,originalFieldName, flatten(b :: a))
        }
        case (MapOfStringAny(a),MapOfStringAny(b)) => {
          val map =b ++ a
          changeElement(originalThing,originalFieldName, map)
        }
        case (MapOfStringAny(a),ListOfMapOfStringAny(b)) =>{
          val map= b.flatMap(p => a ++ p).toMap
          changeElement(originalThing,originalFieldName, map)
        }
        case (a:Any,b:Any) => println("addElement "+thing+" "+fieldName+" "+newValue+"   ---   "+a+" "+b);new Exception
      }
    }else if(fieldName.size>1){
      addElement(thing(fieldName.head).asInstanceOf[Map[String,Any]],originalThing,fieldName.tail,originalFieldName,newValue)
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def addElementWithoutFlatten(thing: Map[String,_],originalThing:Map[String,_],fieldName:List[String],originalFieldName:List[String],newValue:Any): Any ={
    if(fieldName.size==1){
      val v= thing(fieldName.head)
      (v,newValue) match{
        case (ListOfAny(a),b:Any) => {
          changeElement(originalThing,originalFieldName, b :: a)
        }
        case (MapOfStringAny(a),MapOfStringAny(b)) => {
          changeElement(originalThing,originalFieldName, b ++ a)
        }
        case _ => throw new Exception
      }
    }else if(fieldName.size>1){
      addElement(thing(fieldName.head).asInstanceOf[Map[String,Any]],originalThing,fieldName.tail,originalFieldName,newValue)
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def addElementToPosition(thing: Map[String,_],originalthing: Map[String,_],fieldName:List[String],originalFieldName:List[String],newValue:Any,position:Int): Any ={
    if(fieldName.size==1){
      val v= thing(fieldName.head)
      v match{
        case a:List[Any] => {
          val lala = newValue  :: a.drop(position)
          val out =a.take(position)++ lala
          changeElement(originalthing,originalFieldName, out)
        }
      }
    }else if(fieldName.size>1){
      addElementToPosition(thing(fieldName.head).asInstanceOf[Map[String,_]],originalthing,fieldName.tail,originalFieldName,newValue,position)
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def changeElementInPosition(thing: Map[String,_],originalthing: Map[String,_],fieldName:List[String],originalFieldName:List[String],newValue:Any,position:Int): Any ={
    if(fieldName.size==1){
      val v= thing(fieldName.head)
      v match{
        case a:List[Any] => {
          val lala = newValue  :: a.drop(position+1)
          val out =a.take(position)++ lala
          changeElement(originalthing,originalFieldName, out)
        }
      }
    }else if(fieldName.size>1){
      changeElementInPosition(thing(fieldName.head).asInstanceOf[Map[String,_]],originalthing,fieldName.tail,originalFieldName,newValue,position)
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def removeElement(thing: Map[String,_],originalThing: Map[String,_],fieldName:List[String],originalFieldName:List[String],newValue:Any): Any ={
    if(fieldName.size==1){
      val v= thing(fieldName.head)
      (v,newValue) match{
        case (ListOfAny(a),b:Any) => {
          val out=a.filterNot(f => f==b)
          changeElement(originalThing,originalFieldName, out)
        }
        case (MapOfStringAny(a),MapOfStringAny(b)) => {
          changeElement(originalThing,originalFieldName, a -- b.keySet)
        }
        case _ => throw new Exception
      }
    }else if(fieldName.size>1){
      removeElement(thing(fieldName.head).asInstanceOf[Map[String,Any]],originalThing,fieldName.tail,originalFieldName,newValue)
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def removeElementFromPosition(thing: Map[String,_],originalthing: Map[String,_],fieldName:List[String],originalFieldName:List[String],position:Int): Any ={
    if(fieldName.size==1){
      val v= thing(fieldName.head)
      v match{
        case ListOfAny(a)=> {
          val out=a.take(position ) ++ a.drop(position+1)
          changeElement(originalthing,originalFieldName, out)
        }
      }
    }else if(fieldName.size>1){
      removeElementFromPosition(thing(fieldName.head).asInstanceOf[Map[String,Any]],originalthing,fieldName.tail,originalFieldName,position)
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def getElementFromPosition(thing: Map[String,_],originalThing:Map[String,_],fieldName:List[String],originalFieldName:List[String],position:Int): Any ={
    if(fieldName.size==1){
      val v= thing(fieldName.head)
      v match{
        case a:List[Any]  => {
          a(position)
        }
      }
    }else if(fieldName.size>1){
      getElementFromPosition(thing(fieldName.head).asInstanceOf[Map[String,_]],originalThing,fieldName.tail,originalFieldName,position)
    }else{
      throw new Exception("No valid field name given for : "+fieldName)
    }
  }

  def flatten(ls: List[_]): List[_] = ls flatMap {
    case ListOfAny(ms) => flatten(ms)
    case e => List(e)
  }

}
