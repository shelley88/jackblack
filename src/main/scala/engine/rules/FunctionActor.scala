package engine.rules


import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout
import akka.pattern.ask
import shapeless.TypeCase

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

class FunctionActor(modelPath:ActorRef) extends Actor{
  implicit val timeout = Timeout(5 seconds)

  val ListOfMap = TypeCase[List[Map[String,_]]]
  val MapOfStringAny = TypeCase[Map[String,_]]

  import context._
  def receive =runFunction

  def runFunction:Receive ={
    case (MapOfStringAny(function),MapOfStringAny(input),MapOfStringAny(allFunctions), MapOfStringAny(b)) => {
      val s = sender()
      if (function("type") == "value") {
        s ! function("value")
      }
      else if (function("type") == "input") {
        s ! input(function("input").toString)
      }
      else if(function("type")=="subValue"){
        val act =actorOf(Props(new FunctionActor(modelPath)))
        val v=act ? (function("subValue").asInstanceOf[Map[String,Any]], function("subValue").asInstanceOf[Map[String,Any]]("input"),allFunctions,b)
        val value =Await.result(v.mapTo[Map[String,Any]],1000 millis)
        context.stop(act)
        s ! GenericLambda.findElement(value,function("path").asInstanceOf[String].split('.').toList)
      }
      else if(function("type")=="indexValue"){
        val act =actorOf(Props(new FunctionActor(modelPath)))
        val v=act ? (function("indexValue").asInstanceOf[Map[String,Any]], function("indexValue").asInstanceOf[Map[String,Any]]("input"),allFunctions,b)
        val value =Await.result(v,1000 millis)
        context.stop(act)
        s ! GenericLambda.getElementFromPosition(value.asInstanceOf[Map[String,Any]],value.asInstanceOf[Map[String,Any]],function("path").asInstanceOf[String].split('.').toList,function("path").asInstanceOf[String].split('.').toList,function("index").asInstanceOf[Int])
      }
      else if (function("type") == "path") {
        s ! GenericLambda.findElement(b, function("path").asInstanceOf[String].split('.').toList)
      }
      else if (function("type") == "operation") {

        val act = actorOf(Props(new OperationActor(modelPath)))
        val v = act ? (function("operator"), function("parameters"),allFunctions,b)
        v.onComplete {
          case Success(a) => context.stop(act);s ! a
          case Failure(exception) => context.stop(act);s ! exception
        }

      }
      else if (function("type") == "function") {
        val w = actorOf(Props(new FunctionActor(modelPath)))
        val v = w ? (allFunctions(function("function").toString), function("input"),allFunctions,b)
        v.onComplete {
          case Success(a) => context.stop(w);s ! a
          case Failure(exception) => context.stop(w);s ! exception
        }
      }

    }
    case b => sender() ! new Exception
  }
}
