package engine.rules

import shapeless._

object Operations {
  val ListOfStrings = TypeCase[List[String]]
  val ListOfNumbers    = TypeCase[List[Number]]
  val ListOfAny    = TypeCase[List[_]]
  val ListOfList    = TypeCase[List[List[_]]]


  def Equals(list: List[_]):Boolean={
    println("EQUALS "+list)
    if(list.size>0) {
      return list.tail.forall(_ == list.head)
    }else true
  }

  def NotEquals(list: List[_]):Boolean={
    list.distinct.size > 1
  }

  def Different(list: List[_]):Boolean={
    list.distinct.size == list.size
  }

  def Contains(list: List[_]):Boolean={
    list.size>1 && list.tail.head.asInstanceOf[List[Any]].filter(p => list.head.equals(p)).nonEmpty
  }

  def ContainsKey(list: List[_]):Boolean={
    println("**CONTAINSKEY"+list.tail.asInstanceOf[List[Map[String,Any]]].filter(p => {println(p);list.head.asInstanceOf[String].equals(p.keySet.head)})+" "+list.tail)
    list.size>1 && list.tail.asInstanceOf[List[Map[String,Any]]].filter(p => p.keySet.contains(list.head.asInstanceOf[String])).nonEmpty
  }

  def GreaterThan(list: List[_]):Boolean={
    list match {
      case ListOfStrings(b) if list.size>1 => b.sliding(2).forall{case Seq(i:String,j:String) => i > j}
      case ListOfNumbers(a) if list.size>1 => a.sliding(2).forall{case Seq(k:Number,l:Number) => k.doubleValue()>l.doubleValue()}
      case _ => false
    }
  }

  def GreaterOrEquals(list: List[_]):Boolean={
    list match {
      case ListOfStrings(b) if list.size>1 => b.sliding(2).forall{case Seq(i:String,j:String) => i >= j}
      case ListOfNumbers(a) if list.size>1 => a.sliding(2).forall{case Seq(k:Number,l:Number) => k.doubleValue()>=l.doubleValue()}
      case _ => false
    }
  }

  def LessThan(list: List[_]):Boolean ={
    list match {
      case ListOfStrings(b) if list.size>1 => b.sliding(2).forall{case Seq(i:String,j:String) => i < j}
      case ListOfNumbers(a) if list.size>1 => a.sliding(2).forall{case Seq(k:Number,l:Number) => k.doubleValue()<l.doubleValue()}
      case _ => false
    }
  }

  def LessOrEquals(list: List[_]):Boolean={
    list match {
      case ListOfStrings(b) if list.size>1 => b.sliding(2).forall{case Seq(i:String,j:String) => i <= j}
      case ListOfNumbers(a) if list.size>1 => a.sliding(2).forall{case Seq(k:Number,l:Number) => k.doubleValue()<=l.doubleValue()}
      case _ => false
    }
  }

  def Add(list: List[_]):Any={
    println("ADD "+list)
    list match {
      case ListOfNumbers(a) if list.nonEmpty => a.foldRight(0.00)(_.doubleValue() + _)
      case ListOfStrings(b) if list.nonEmpty => b.foldRight("")(_ + _)
      case _ => new Exception("ADD failed for : "+list)
    }
  }

  def Subtract(list: List[_]):Any={
    list match {
      case ListOfNumbers(a) if list.size>1 => a.tail.foldLeft(a.head.doubleValue())(_ - _.doubleValue())
      case ListOfStrings(b) if list.size>1 => b.tail.foldLeft(b.head.toString)(_.replace(_,""))
      case _ => throw new Exception
    }
  }

  def Multiply(list: List[_]):Any={
    list match {
      case ListOfNumbers(a) if (list.nonEmpty) => a.foldRight(1.0)(_.doubleValue() * _)
      case _ => throw new Exception
    }
  }

  def Divide(list: List[_]):Any={
    list match {
      case ListOfNumbers(a) if list.size>1 => a.tail.foldLeft(a.head.doubleValue())(_ / _.doubleValue())
      case _ => throw new Exception
    }
  }

  def GetFirst(list: List[_]):Any={
    if (list.nonEmpty && list.head != null) {
      list.head match {
        case ListOfList(a) if a.nonEmpty && a.head.nonEmpty=> a.head
        case ListOfAny(a) if a.nonEmpty=> a.head
        case b => b
      }
    }else List()
  }

  def GetX(list: List[_]):Any={
    list match {
      case ListOfAny(a) if a.size>1 && a.head.isInstanceOf[Double] && list.tail.head.asInstanceOf[List[Any]].size >= a.head.asInstanceOf[Double].toInt && a.head.asInstanceOf[Double] >= 0=> a.tail.head.asInstanceOf[List[_]](a.head.asInstanceOf[Double].toInt)
      case ListOfAny(a) if a.size>1 && a.head.isInstanceOf[Int] && list.tail.head.asInstanceOf[List[Any]].size >= a.head.asInstanceOf[Int] && a.head.asInstanceOf[Int] >= 0=> a.tail.head.asInstanceOf[List[_]](a.head.asInstanceOf[Int])

      case b => Map()
    }
  }
  def GetPositionInList(list: List[_]):Any={
    println(list)
    list match {
      case ListOfAny(a) if a.size==3 && a(2).isInstanceOf[List[Map[String,Any]]] => list(2).asInstanceOf[List[Map[String,Any]]].indexWhere(p => p(list.head.asInstanceOf[String]) == list(1))
      case b => -1
    }
  }

  def Shuffle (list:List[_]):List[Any]={
    list match {
      case ListOfList(b) if b.nonEmpty && b.head.nonEmpty => GenericLambda.flatten(scala.util.Random.shuffle(b.head))
      case ListOfAny(a) if a.nonEmpty => GenericLambda.flatten(scala.util.Random.shuffle(a))
      case b => b
    }
  }

  def Random (list:List[Any]):Any = {
    list match {
      case a:List[Int] if a.length == 2 => val r=scala.util.Random;a.head+r.nextInt(a(1)-a.head +1)
      case _ => throw new Exception
    }
  }
  def Length (list:List[Any]):Any = {
    list match {
      case a:List[Any] => a.length
      case _ => throw new Exception
    }
  }
  def Modulo (list:List[Any]):Any = {
    list match {
      case a:List[Int] if a.length == 2 => a.head % a(1)
      case _ => throw new Exception
    }
  }
  def ErrorHandler(list:List[_]):Any={
}

  def getListOperation(string: String):List[_] => Any ={
    string match {
      case "Add" => Add
      case "Subtract" => Subtract
      case "Multiply" => Multiply
      case "Divide" => Divide
      case "GreaterThan" => GreaterThan
      case "GreaterOrEquals" => GreaterOrEquals
      case "LessThan" => LessThan
      case "LessOrEquals" => LessOrEquals
      case "Equals" => Equals
      case "NotEquals" => NotEquals
      case "Different" => Different
      case "Shuffle" => Shuffle
      case "Random" => Random
      case "GetFirst" => GetFirst
      case "GetPosition" => GetX
      case "GetPositionInList" => GetPositionInList
      case "Length" => Length
      case "Modulo" => Modulo
      case "Contains" => Contains
      case "ContainsKey" => ContainsKey
      case g => println("********** "+g);ErrorHandler
    }
  }


}