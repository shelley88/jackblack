package api

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpRequest}
import akka.http.scaladsl.server.{HttpApp, Route}
import akka.pattern.ask
import akka.util.Timeout
import api.CassandraStartUp._
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import runner.ActionActor

import scala.collection.parallel.ParMap
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.Source

object WebServer extends HttpApp {
  implicit val timeout: Timeout = Timeout(4000 seconds)
  CassandraStartUp
  case class Run(model:String, functions:String, rules:String, runner:String, modFunctions:List[String], modRules:List[String], modRunner:List[String], result:String, gameName:String, id:Int)
  case class Session(actor:ActorRef,saved:Run)
  var sessionMap = ParMap[String, Session]()
  var decisionMap= ParMap[String,(String,List[(String,Any,Any)],List[Any],List[Any],Map[String,Any],ActorRef)]()//DECISION,wait,rulesResult,model,send
  var system: ActorSystem = ActorSystem("ActorSystem")
  var ruleEngineSessionMap= ParMap[String, Session]()
  val mapper = new ObjectMapper
  mapper.registerModule(DefaultScalaModule)
  var session = maxSessions
  var rulesActor:ActorRef =_
//  def setRulesActor(rulesActor:ActorRef)

  def runSimulationByName(name:String):Route={
    val dbResponse = getSimulationFromDb(name)

    val dbSimulation = Await.result(dbResponse.mapTo[List[Run]],1 second).last

    session += 1
    val thisSession = session

    val functionJSON = dbSimulation.functions
    val rulesJSON = dbSimulation.rules
    val runnerJSON = dbSimulation.runner

    val inputJSON = dbSimulation.model
    val example2: ActorRef = system.actorOf(Props(new ActionActor("/user/ActionActor" + thisSession + "/Model")), "ActionActor" + thisSession)
    val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
    example2 ! thing

    val functions = mapper.readValue(functionJSON, classOf[Map[String, Any]])
    val rules = mapper.readValue(rulesJSON, classOf[Map[String, Any]])
    val runner = mapper.readValue(runnerJSON, classOf[Map[String, Any]])

    example2 ! functions("functions")
    example2 ! rules("rules")
    example2 ! runner("runner")


    val run = example2 ? "run simulation"

    val runResult = Await.result(run, 20 seconds)

    val model = example2 ? "getModel"
    val result = Await.result(model, 20 seconds)

    println("****  session " + thisSession + "*****\n" + result + "\n************************")

    system.stop(example2)
    val t = JsonConverter.toJson(result)
    sessionMap = sessionMap + (thisSession.toString -> Session(example2,Run(inputJSON,functionJSON,rulesJSON,runnerJSON,List(), List(), List(), t, "ScratchCards",thisSession)))

    saveOut(Run(inputJSON,functionJSON,rulesJSON,runnerJSON,List(), List(), List(), t, "ScratchCards",thisSession))

    complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, t))
  }

  def loadSimulationByName(name:String):Int={
    val dbResponse = getSimulationFromDb(name)
    val dbSimulation = Await.result(dbResponse.mapTo[List[Run]],1 second).last
    session += 1
    val thisSession = session

    val functionJSON = dbSimulation.functions
    val rulesJSON = dbSimulation.rules
    val runnerJSON = dbSimulation.runner

    val inputJSON = dbSimulation.model
    val example2: ActorRef = system.actorOf(Props(new ActionActor("/user/ActionActor" + thisSession + "/Model")), "ActionActor" + thisSession)
    //            sessionMap += (thisSession -> (example2,inputJSON,functionJSON,rulesJSON,runnerJSON,List(),List(),List(),List(),"",simulation,thisSession))
    val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
    example2 ! thing

    val functions = mapper.readValue(functionJSON, classOf[Map[String, Any]])
    val rules = mapper.readValue(rulesJSON, classOf[Map[String, Any]])
    val runner = mapper.readValue(runnerJSON, classOf[Map[String, Any]])

    example2 ! functions("functions")
    example2 ! rules("rules")
    example2 ! runner("runner")

    sessionMap =sessionMap + (thisSession.toString -> Session(example2,Run(inputJSON,functionJSON,rulesJSON,runnerJSON,List(),List(),List(),"",name,thisSession)))

    thisSession
  }

  override def routes: Route = {
    path("runScratchCards") {
      get {
        entity(as[HttpRequest]) { input =>
          runSimulationByName("ScratchCards")
        }
      }
    } ~
      path("runBlackJack") {
        get {
          entity(as[HttpRequest]) { input =>
            runSimulationByName("BlackJack")
          }
        }
      } ~
      path("runBlackJackPro") {
        get {
          entity(as[HttpRequest]) { input =>
            runSimulationByName("BlackJackPro")
          }
        }
      } ~
      path("loadSimulation") {
        get {
          entity(as[HttpRequest]) { input =>

            val simulation = input.headers.filter(p => p.name() == "simulation").head.value()
           val sess=loadSimulationByName(simulation)
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "{\"session\" : "+sess+"}"))

          }
        }
      } ~
      path("saveSimulation") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            val simulation = input.headers.filter(p => p.name() == "simulation").head.value()
            if (sessionMap.contains(inputSession)) {
              saveOut(sessionMap(inputSession).saved)
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Rules Set"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("runSimulation") {
        post {
          entity(as[HttpRequest]) { input =>
            var multiSessionMap: ParMap[String, ActorRef] = ParMap[String, ActorRef]()
            val concurrent = input.headers.filter(p => p.name() == "concurrent").head.value().toInt
            val simulation = input.headers.filter(p => p.name() == "simulation").head.value()

            val functionJSON = Source.fromResource(simulation + "/functions.json").getLines.fold("")(_ + _)
            val rulesJSON = Source.fromResource(simulation + "/rules.json").getLines.fold("")(_ + _)
            val runnerJSON = Source.fromResource(simulation + "/runner.json").getLines.fold("")(_ + _)

            (1 to concurrent).map { session =>
              val inputJSON = Source.fromResource(simulation + "/model.json").getLines.fold("")(_ + _)
              val example2: ActorRef = system.actorOf(Props(new ActionActor("/user/ActionActor" + session + "/Model")), "ActionActor" + session)
              multiSessionMap += (session.toString -> example2)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example2 ! thing

              val functions = mapper.readValue(functionJSON, classOf[Map[String, Any]])
              val rules = mapper.readValue(rulesJSON, classOf[Map[String, Any]])
              val runner = mapper.readValue(runnerJSON, classOf[Map[String, Any]])

              example2 ! functions("functions")
              example2 ! rules("rules")
              example2 ! runner("runner")

              val run = example2 ? "run simulation"

              val runResult = Await.result(run, 20 seconds)

              val model = example2 ? "getModel"
              val result = Await.result(model, 20 seconds)

              println("****  session " + session + "*****\n" + result + "\n************************")

              system.stop(example2)
            }
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Simulation finished"))

          }
        }
      } ~
      path("playSimulation") {
        get {
          entity(as[HttpRequest]) { input =>
            val session = input.headers.filter(p => p.name() == "session").head.value()

            val example2 = sessionMap(session).actor

            example2 ! "player"


            val run = example2 ? "run simulation"

            val runResult = Await.result(run.mapTo[(String,List[(String,Any,Any)],List[Any],List[Any],Map[String,Any],ActorRef)], 20 seconds)

            decisionMap=decisionMap+(session.toString, runResult)

            println(Map("decide" -> runResult._2.map(p => Map(p._1 -> p._2))))
            println("****  session " + session + "*****\n" + JsonConverter.toJson(runResult._2.map(p => Map("decision"->p._1.replace("decision", "") ,"rtp" ->p._2))) + "\n************************")

            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, JsonConverter.toJson(runResult._2.map(p => Map("decision"->p._1.replace("decision", "") ,"rtp" ->p._2))).toString()))

          }
        }
      } ~
      path("makeDecision") {
        post {
          entity(as[HttpRequest]) { input =>
            val session = input.headers.filter(p => p.name() == "session").head.value()
            val decision = input.headers.filter(p => p.name() == "decision").head.value()

            val example2 = sessionMap(session).actor

            val run = example2 ? (decisionMap(session.toString)._1,List(decision),decisionMap(session.toString)._3,decisionMap(session.toString)._4,decisionMap(session.toString)._5,None)

            val runResult = Await.result(run, 20 seconds)

            val sendPlayer = if(runResult.isInstanceOf[(String,List[(String,Any,Any)],List[Any],List[Any],Map[String,Any],ActorRef)]){
              JsonConverter.toJson( runResult.asInstanceOf[(String,List[(String,Any,Any)],List[Any],List[Any],Map[String,Any],ActorRef)]._2.map(p => Map("decision"->p._1.replace("decision", "") ,"rtp" ->p._2)))
            }else{
              val model = example2 ? "getModel"
              val result = Await.result(model, 20 seconds)
              JsonConverter.toJson(result)
            }

            println("****  session " + session + "*****\n" + sendPlayer + "\n************************")

            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, sendPlayer.toString))

          }
        }
      } ~
      path("setModel") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(text/plain; charset=UTF-8,", "").replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            session += 1
            val thisSession = session
            val gameName = input.headers.filter(p => p.name() == "gameName").head.value()
            val example2: ActorRef = system.actorOf(Props(new ActionActor("/user/ActionActor" + thisSession + "/Model")), "ActionActor" + thisSession)
            sessionMap =sessionMap + (thisSession.toString -> Session(example2,Run(inputJSON,"","","",List(),List(),List(),"",gameName,thisSession)))
            val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
            example2 ! thing
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Model Set"))
          }
        }
      } ~
      path("updateModel") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(model=inputJSON)))
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! thing
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Model Updated"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("getModel") {
        get {
          entity(as[HttpRequest]) { input =>
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val model = example.actor ? "getModel"
              val result = Await.result(model, 20 seconds)
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, JsonConverter.toJson(result).toString))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("setRules") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(rules=inputJSON)))
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! thing("rules")
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Rules Set"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("setRulesRules") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (ruleEngineSessionMap.contains(inputSession)) {
              ruleEngineSessionMap=ruleEngineSessionMap+(inputSession.toString -> Session(ruleEngineSessionMap(inputSession).actor,ruleEngineSessionMap(inputSession).saved.copy(rules=inputJSON)))
              val example = ruleEngineSessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! thing("rules")
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Rules Set"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("modifyRules") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("modifyRules", thing("modifyRules"))
              val list = sessionMap(inputSession).saved.modRules
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modRules=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Rules Modified"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("deleteRules") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("deleteRules", thing("deleteRules"))
              val list = sessionMap(inputSession).saved.modRules
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modRules=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Rules Deleted"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("addRules") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("addRules", thing("addRules"))
              val list = sessionMap(inputSession).saved.modRules
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modRules=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Rules Added"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("runRules") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (ruleEngineSessionMap.contains(inputSession)) {
              val example = ruleEngineSessionMap(inputSession)
              val exe = example.actor ? "run"
              val result = Await.result(exe, 20 seconds)
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, result.toString))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("runRulesEngine") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val exe = example.actor ? "run"
              val result = Await.result(exe, 20 seconds)
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, result.toString))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("setFunctions") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").dropRight(1)
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! thing("functions")
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(functions=inputJSON)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Functions Set"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("setFunctionsRule") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").dropRight(1)
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (ruleEngineSessionMap.contains(inputSession)) {
              val example = ruleEngineSessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! thing("functions")
              ruleEngineSessionMap=ruleEngineSessionMap+(inputSession.toString -> Session(ruleEngineSessionMap(inputSession).actor,ruleEngineSessionMap(inputSession).saved.copy(functions=inputJSON)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Functions Set"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("modifyFunctions") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("modifyFunctions", thing("modifyFunctions"))
              val list = sessionMap(inputSession).saved.modFunctions
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modFunctions=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Functions Modified"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("deleteFunctions") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("deleteFunctions", thing("deleteFunctions"))
              val list = sessionMap(inputSession).saved.modFunctions
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modFunctions=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Functions Deleted"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("addFunctions") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("addFunctions", thing("addFunctions"))
              val list = sessionMap(inputSession).saved.modFunctions
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modRules=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Functions Added"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("setAction") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! thing("runner")
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(runner=inputJSON)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Set"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
    path("lala"){
      post{
        val runner=Source.fromResource("RuleEngineTimer"+ "/runner.json").getLines.fold("")(_ + _)
        println("PPPPPP "+runner )
        rulesActor ! mapper.readValue(runner, classOf[Map[String, Any]])("runner")
        complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Set"))
      }
    }~
      path("setFEAction") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
//            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
//            if (sessionMap.contains(inputSession)) {
//              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              val thing2=thing("Actions").asInstanceOf[List[Map[String,Any]]].map{p =>
                if (p.keySet.contains("Timer")){
                  println("timer "+p)
                  (p.getOrElse("ActionName", "noNameTimer") -> "lala")
                }
                else{
                  println("normal "+p)
                  (p.getOrElse("ActionName", "noNameNormal") -> "lalalala")
                }
              }.toMap
            println(thing2)
            thing2.foreach(p => println(p))
//              example.actor ! thing("runner")
//              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(runner=inputJSON)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Set"))
//            } else {
//              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
//            }
          }
        }
      } ~
      path("setActionRule") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (ruleEngineSessionMap.contains(inputSession)) {
              val example = ruleEngineSessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! thing("runner")
              ruleEngineSessionMap=ruleEngineSessionMap+(inputSession.toString -> Session(ruleEngineSessionMap(inputSession).actor,ruleEngineSessionMap(inputSession).saved.copy(runner=inputJSON)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Set"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("modifyActionRule") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              rulesActor ! ("modifyAction", thing("modifyAction"))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Modified"))

          }
        }
      } ~
      path("modifyAction") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("modifyAction", thing("modifyAction"))
              val list = sessionMap(inputSession).saved.modRunner
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modRunner=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Modified"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("deleteAction") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("deleteAction", thing("deleteAction"))
              val list = sessionMap(inputSession).saved.modRunner
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modRunner=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Deleted"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("addAction") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              example.actor ! ("addAction", thing("addAction"))
              val list = sessionMap(inputSession).saved.modRunner
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(modRunner=inputJSON :: list)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Added"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("addActionRule") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputJSON = input._4.toString.replace("HttpEntity.Strict(application/json,", "").replace(")", "")
              val thing = mapper.readValue(inputJSON, classOf[Map[String, Any]])
              rulesActor ! ("addAction", thing("addAction"))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Action Added"))
          }
        }
      } ~
      path("runAction") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (sessionMap.contains(inputSession)) {
              val example = sessionMap(inputSession)
              val exe = example.actor ? "run simulation"
              val result = Await.result(exe, 20 seconds)
              val t = JsonConverter.toJson(result)
              sessionMap=sessionMap+(inputSession.toString -> Session(sessionMap(inputSession).actor,sessionMap(inputSession).saved.copy(result=t)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, result.toString))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("ruleEngineRunAction") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (ruleEngineSessionMap.contains(inputSession)) {
              val example = ruleEngineSessionMap(inputSession)
              val exe = example.actor ? "run simulation"
              val result = Await.result(exe, 20 seconds)
              val t = JsonConverter.toJson(result)
              ruleEngineSessionMap=ruleEngineSessionMap+(inputSession.toString -> Session(ruleEngineSessionMap(inputSession).actor,ruleEngineSessionMap(inputSession).saved.copy(result=t)))
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, result.toString))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }
        }
      } ~
      path("stop") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (ruleEngineSessionMap.contains(inputSession)) {
              system.stop(ruleEngineSessionMap(inputSession).actor)
              ruleEngineSessionMap -= inputSession
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Simulation stopped"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }

        }
      } ~
      path("ruleEngineStop") {
        post {
          entity(as[HttpRequest]) { input =>
            val inputSession = input.headers.filter(p => p.name() == "session").head.value()
            if (ruleEngineSessionMap.contains(inputSession)) {
              system.stop(ruleEngineSessionMap(inputSession).actor)
              ruleEngineSessionMap -= inputSession
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "Simulation stopped"))
            } else {
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "You don't have a session started, dumb ass!!!"))
            }
          }

        }
      } ~
      path("sessions") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, sessionMap.keySet.toString()))
        }
      }~
    path("ruleEngineSessions") {
      get {
        complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, ruleEngineSessionMap.keySet.toString()))
      }
    }
  }
}