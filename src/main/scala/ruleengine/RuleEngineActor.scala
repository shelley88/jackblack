package ruleengine

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import api.JsonConverter
import engine.rules.{ModelActor, RuleActor}
import runner.MorphActor
import shapeless.TypeCase

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

class RuleEngineActor(modelPath:String,writerActor: ActorRef) extends Actor{
  val ListOfMap = TypeCase[List[Map[String,_]]]
  val ListOfMapOfMap = TypeCase[List[Map[String,Map[String,_]]]]
  val MapOfStringAny = TypeCase[Map[String,_]]
  val MapOfMap = TypeCase[Map[String,Map[String,_]]]
  val ListOfString = TypeCase[List[String]]
  val ListOfTuple = TypeCase[List[(String,_)]]
  implicit val timeout = Timeout(4000 seconds)
  val modelActor: ActorRef=context.actorOf(Props(new ModelActor),name = "Model")
  val morph: ActorRef = context.actorOf(Props(new MorphActor(modelActor)),"Morph")

  import context._

  def receive= setModel

  def setModel:Receive ={
    case MapOfStringAny(a) => {

      modelActor ! a
      become(setFunctions)
    }
    case b => println("WRONG "+b)
  }
  def setFunctions:Receive={
    case MapOfStringAny(a) => {
      become(setRules(a))
    }
  }

  def setRules(functions:Map[String,Any]):Receive ={
    case MapOfStringAny(a) => {
      val ruleActors=a.map(p => {
        val actor= actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2,functions)
        actor
      })
      become(setAction(functions,ruleActors.toList))
    }
    case b => println("6not matching type "+b)
  }

  def setAction(functions:Map[String,Any], ruleActors:List[ActorRef]):Receive ={
    case MapOfMap(rules) =>{
      val send = sender()
      val actionActors=rules.filter(_._2.keySet.contains("rule")).map{ p=>
        val actor=actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2("rule") ,functions)
        actor
      }
      val timerActors=rules.filter(_._2.keySet.contains("timerRule")).map{ p=>
        val actortime=actorOf(Props(new TimeActor(p._2("timer").asInstanceOf[Map[String,Any]]("initialDelay").asInstanceOf[Int],p._2("timer").asInstanceOf[Map[String,Any]]("interval").asInstanceOf[Int],self)),name = "timer"+p._1)
        actortime ! (p._2("timerRule") ,functions)
        val actorTimeRule = actorOf(Props(new RuleActor(modelActor)),name = "timerRule"+p._1)
        actorTimeRule ! (p._2("stopRule") ,functions)
        val actor=actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2("timerRule") ,functions)
        (actor,actorTimeRule,actortime,p._1)
      }
      println("create actionActors "+actionActors + rules)
      become(receiveWithParam(functions,rules,ruleActors,actionActors.toList,timerActors.toList/*stopActors.toList,decisionActionActors.toList,"auto"*/))
    }
  }

  def receiveWithParam(functions:Map[String,Any],rules:Map[String,Map[String,Any]],rulesActors:List[ActorRef],actionActors:List[ActorRef],timers:List[(ActorRef,ActorRef,ActorRef,String)]):Receive ={

    case ("modifyAction",MapOfMap(action)) =>{
      actionActors.filter(actor=> action.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",action(actor.path.name)("rule"))})

      val newRules=rules.map(rule => if(action.contains(rule._1))(rule._1 -> action(rule._1))  else (rule._1 -> rule._2))

      become(receiveWithParam(functions,newRules,rulesActors,actionActors,timers))
    }
    case ("modifyFunctions",MapOfStringAny(action)) => {
      val newFunctions=functions.map(rule => if(action.contains(rule._1))(rule._1 -> action(rule._1))  else (rule._1 -> rule._2))

      actionActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,actionActors,timers))
    }
    case ("modifyRules",MapOfStringAny(action)) =>{
      rulesActors.filter(actor=> action.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",action(actor.path.name))})
    }
    case ("addAction",MapOfMap(action)) =>{
      val newAction = action.filter(actor => actor._2.keySet.contains("rule")).map{
        actor => val newActor=actorOf(Props(new RuleActor(modelActor)),name = actor._1)
          newActor ! (actor._2("rule") ,functions)
          newActor
      }
//      val addedTimers = action.filter(actor => actor._2.keySet.contains("timer")).map{
//        actor => val newActor=actorOf(Props(new RuleActor(modelActor)),name = actor._1)
//          newActor ! (actor._2("timer") ,functions)
//          newActor
//      }

      val newRules=action.map(rule => rule._1 -> rule._2)

      become(receiveWithParam(functions,rules ++ newRules,rulesActors,actionActors ++ newAction,timers /*++ addedTimers,stopActors ++ newStop,decisionActionActors,whoDecides*/))
    }
    case ("addFunctions",MapOfStringAny(action)) =>{
      val newFunctions=functions ++ action

      actionActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,actionActors,timers/*,stopActors,decisionActionActors,whoDecides*/))
    }
    case ("addRules",MapOfStringAny(action)) =>{
      val newActors=action.map(rule => {
        val actor =actorOf(Props(new RuleActor(modelActor)),name = rule._1)
        actor ! (rule._2,functions)
        actor
      })
      become(receiveWithParam(functions,rules,rulesActors ++ newActors.toList,actionActors,timers/*,stopActors,decisionActionActors,whoDecides*/))
    }
    case ("deleteAction",ListOfString(action)) =>{
      val (actionActorDeleted,actionActorKept) =actionActors.partition(actor=> action.contains(actor.path.name))

      val newRules=rules.filterNot(rule => action.contains(rule._1))

      actionActorDeleted.foreach(actor => {system.stop(actor);context.watch(actor)})

      become(receiveWithParam(functions,newRules,rulesActors,actionActorKept,timers/*,stopActorKept,decisionActionActors,whoDecides*/))
    }
    case ("deleteFunctions",ListOfString(action)) =>{
      val newFunctions=functions.filterNot(rule => action.contains(rule._1))

      actionActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,actionActors,timers/*,stopActors,decisionActionActors,whoDecides*/))
    }
    case ("deleteRules",ListOfString(action)) =>{
      val (deletedActors,keptActors)=rulesActors.partition(p=> action.contains(p.path.name))
      deletedActors.map(actor => {system.stop(actor);context.watch(actor);actor})
      become(receiveWithParam(functions,rules,keptActors,actionActors,timers/*,stopActors,decisionActionActors,whoDecides*/))
    }
    case ("addTimer")=>{
      //create timers by creating TimerActors
    }
      //modify,
    case ("timer", ListOfMap(input))=>{
      //this is called when timer executes in TimerActor, creates rule, runs and destroys
      val in = input.map(p => (p.keySet.head,p(p.keySet.head)))
      println(in)
      val send = sender()
      println("****** INPUT "+in)
      val thing = modelActor ? "getModel"
      val model = Await.result(thing.mapTo[Map[String, Any]], 1000 millis)

      val w = timers.map(p => {
        println(p)
        println("run "+model+" "+in)
        p._1 ? ("run", model, in)
      })
      val future = Future.sequence(w)
      val wait = Await.result(future, 20000 millis)
      println("wait "+wait)

      val thing4 = modelActor ? "getModel"
      val model4 = Await.result(thing4.mapTo[Map[String, Any]], 1000 millis)
      //
      //Normal side effects to be run
      wait.foreach(p => {
        if (p.asInstanceOf[(String, Boolean,Boolean)]._2) {
          val q = rules(p.asInstanceOf[(String, Boolean,Boolean)]._1)
          q("side-effects") match {
            case MapOfStringAny(a) => println("should be a list side-effects")
            case ListOfMap(b) => {
              val fut = q("side-effects").asInstanceOf[List[Map[String, Any]]].map(f => {
                println("side-effects")
                var path=List("")
                if(f("path").isInstanceOf[String]){

                  path = f("path").toString.split('.').toList
                }else{
                  val root = f("path").asInstanceOf[Map[String,Any]]("root").toString.split('.').toList
                  println("ROOT "+root)
                  path = root ++ in.filter(p=> p._1==f("path").asInstanceOf[Map[String,String]]("input")).head._2.toString().split('.').toList
                  println("PATH "+path)
                }

                val value = f("value")



                println("value + path "+value+" "+path)

                val act = actorOf(Props(new RuleActor(modelActor)))



                act ! (value, functions)
                val result = act ? ("run", model4, in)
                val v = Await.result(result, 20000 millis)
                println("has run rules "+v)
                context.stop(act)

                val typeOp = f("type")
                val v2 = morph ? (typeOp, path, v.asInstanceOf[(Any, Any,Any)]._2, model4)
                val p = Await.result(v2.mapTo[Map[String,Any]], 10000 millis)
                println("******** return "+p)
                val thing5 = modelActor ? "getModel"
                val model5 = Await.result(thing5.mapTo[Map[String, Any]], 1000 millis)
                println("NEW MODEL "+model5)
                val t = JsonConverter.toJson(p("output"))
                writerActor ! model5+" --- "+t
              })
            }
          }
        }
      })
      //stop timer if stop timer rule is true

      val thing2 = modelActor ? "getModel"
      val model2 = Await.result(thing2.mapTo[Map[String, Any]], 1000 millis)

      val w2 = timers.map(p => {
        println(p)
        println("run "+model2+" "+in)
        p._2 ? ("run", model2, in)
      })
      val future2 = Future.sequence(w2)
      val wait2 = Await.result(future2, 20000 millis)
      println("wait "+wait2)

      val thing5 = modelActor ? "getModel"
      val model5 = Await.result(thing5.mapTo[Map[String, Any]], 1000 millis)

      wait2.foreach(p => {
        if (p.asInstanceOf[(String, Boolean, Boolean)]._2) {

          println("**************************\n************** TIMER SHOULD STOP\n************************")
          println("TIMERS"+timers)
          println("P"+p)
            timers.find(t => "timerRule"+t._4 == p.asInstanceOf[(String, Boolean, Boolean)]._1).map{
              m => m._3 ! "stop"
            }
        }
      })

    }
    case ("input", ListOfMap(input))  => {
      val in = input.map(p => (p.keySet.head,p(p.keySet.head)))
      println(in)
      val send = sender()
      println("****** INPUT "+in)
      val thing = modelActor ? "getModel"
      val model = Await.result(thing.mapTo[Map[String, Any]], 1000 millis)

//      val rulesRun = rulesActors.map(p => {
//        p ? ("run", model)
//      })
//      val rulesFuture = Future.sequence(rulesRun)
//      val rulesResult = Await.result(rulesFuture, 10000 millis)
      println("actionActors "+actionActors )

      val w = actionActors.map(p => {
        println("run "+model+" "+in)
        p ? ("run", model, in)
      })
      val future = Future.sequence(w)
      val wait = Await.result(future, 20000 millis)
      println("wait "+wait)

        val thing4 = modelActor ? "getModel"
        val model4 = Await.result(thing4.mapTo[Map[String, Any]], 1000 millis)
        //
        //Normal side effects to be run
        wait.foreach(p => {
          if (p.asInstanceOf[(String, Boolean,Boolean)]._2) {
            val q = rules(p.asInstanceOf[(String, Boolean,Boolean)]._1)
            q("side-effects") match {
              case MapOfStringAny(a) => println("should be a list side-effects")
              case ListOfMap(b) => {
                val fut = q("side-effects").asInstanceOf[List[Map[String, Any]]].map(f => {
                  println("side-effects")
                  var path=List("")
                  if(f("path").isInstanceOf[String]){

                    path = f("path").toString.split('.').toList
                  }else{
                    val root = f("path").asInstanceOf[Map[String,Any]]("root").toString.split('.').toList
                    println("ROOT "+root)
                    path = root ++ in.filter(p=> p._1==f("path").asInstanceOf[Map[String,String]]("input")).head._2.toString().split('.').toList
                    println("PATH "+path)
                  }

                  val value = f("value")



                  println("value + path "+value+" "+path)

                  val act = actorOf(Props(new RuleActor(modelActor)))
                  act ! (value, functions)
                  val result = act ? ("run", model4, in)
                  val v = Await.result(result, 20000 millis)
                  println("has run rules "+v)
                  context.stop(act)

                  val typeOp = f("type")
                  val v2 = morph ? (typeOp, path, v.asInstanceOf[(Any, Any,Any)]._2, model4)
                  val p = Await.result(v2.mapTo[Map[String,Any]], 10000 millis)
                  println("******** return "+p)
                  val thing5 = modelActor ? "getModel"
                  val model5 = Await.result(thing5.mapTo[Map[String, Any]], 1000 millis)
                  println("NEW MODEL "+model5)
                  val t = JsonConverter.toJson(p("output"))
                  writerActor ! model5+" --- "+t
                })
              }
            }
          }
        })

    }

    case MapOfStringAny(a) => {
      val send =sender()
      context.actorSelection(self.path.address+modelPath) ! a
    }
    case "getModel" => {
      val send =sender()
      val model =context.actorSelection(self.path.address+modelPath) ? "getModel"
      model.onComplete{
        case Success(value) => send ! value
        case Failure(exception) => send ! exception
      }

    }
    case "run" => {
      val send =sender()
      val thing = modelActor ? "getModel"
      val model = Await.result(thing,1000 millis)
      val w= rulesActors.map(p=> {p ? ("run",model)})
      Future.sequence(w).onComplete {
        case Success(a) => send ! a
        case Failure(b) => send ! b
      }
    }
    case c => println("error in action actor"+c)
  }

}
