package ruleengine

import java.util.UUID

import akka.actor.Actor

class KafkaWriterActor extends Actor{
    import java.util.Properties

    import org.apache.kafka.clients.producer._

    val  props = new Properties()
    props.put("bootstrap.servers", "PLAINTEXT://localhost:9092")

    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)

    val TOPIC="test2"

  override def receive: Receive = {
    case a => {
      val record = new ProducerRecord(TOPIC, "key", a.toString+"\n")
      producer.send(record)
    }
  }
}
