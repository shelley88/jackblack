package ruleengine
import akka.pattern.{ask, pipe}
import akka.actor.{Actor, ActorRef, Props, Timers}
import akka.util.Timeout
import engine.rules.{FunctionActor, GenericLambda, OperationActor, RuleActor}
import shapeless.TypeCase

import scala.concurrent.Await
import scala.concurrent.duration._
object MyActor {
  case object TickKey
  case object FirstTick
  case object Tick
}

class TimeActor(initialDelay:Int,interval:Int,ruleEngineActor:ActorRef) extends Actor with Timers{
  val MapOfStringAny = TypeCase[Map[String,_]]
  val ListType = TypeCase[List[(String,_)]]
  import context._
  implicit val timeout = Timeout(15 seconds)

  import MyActor._
  println("TIMER ACTOR")
  def receive =setRule

  def setRule:Receive={
    case (MapOfStringAny(input),MapOfStringAny(functions)) => {
      timers.startSingleTimer(TickKey, FirstTick, initialDelay.millis)
      become(processRule(input,functions))
    }
//    case _=> println("*****lala*****")
  }
  def processRule(input:Map[String,Any], func:Map[String,Any]): Receive = {//https://doc.akka.io/docs/akka/2.5/actors.html?language=java#timers-scheduled-messages
    //case _/*initial delay,timeout for scheduler,times, and rule, and recover strategy*/ =>
//    case (interval:Int,rule :Map[String,Any], recovery:String)=>{
//      timers.startPeriodicTimer(TickKey, Tick, interval.millis)
//    }
//
//    case FirstTick =>{ println("FIRST TICK")
////      timers.startPeriodicTimer(TickKey, Tick, interval.millis)
//    }
    case FirstTick =>{ println("FIRST TICK")
      ruleEngineActor ! ("timer" ,List())
      timers.startPeriodicTimer(TickKey, Tick, interval.millis)
    }
    case Tick =>{ruleEngineActor ! ("timer" ,List());println("TICK")}

    case "stop" => println("TIMERSTOP "+timers);timers.cancelAll()
  }
}
