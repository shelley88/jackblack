package ruleengine

import java.util
import java.util.Properties

import akka.actor.{Actor, ActorRef}
import com.fasterxml.jackson.annotation.JsonFormat.Feature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.getquill.ast.Constant
import org.apache.kafka.clients.consumer.KafkaConsumer

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._


class KafkaReaderActor(ruleEngineActor: ActorRef) extends Actor{
  import context.system
  val mapper = new ObjectMapper
  mapper.registerModule(DefaultScalaModule)

  import ExecutionContext.Implicits.global
  override def receive: Receive = {
    case ("start",topic:String,properties:Map[String,String],regex:String,replacement:String)=> {

      val TOPIC = topic

      val  props = new Properties()

      properties.foreach(p => props.put(p._1,p._2))

      val consumer = new KafkaConsumer[String, String](props)

      println("******!!!!!!!"+consumer.listTopics())

      consumer.subscribe(util.Collections.singletonList(TOPIC))

      system.scheduler.schedule(0 millis,50 millis){
        val records=consumer.poll(50)
        for (record<-records.asScala){
          println("\n\n\n*****"+record.value().toString.replaceAll("[^A-Za-z0-9 ()]", "")+"\n\n\n")

//          if (record.value().toString.contains("DEBUG")){
//            val json=mapper.readValue("[{\"level\":\"DEBUG\"},{\"amount\":1},{\"message\":\""+record.value().toString.replaceAll("[^A-Za-z0-9 ]", "")+"\"}]", classOf[List[(String, Any)]])
//            ruleEngineActor ! ("input" ,json)
//            println("DEBUG MESSAGE "+record.value())
//          }
//          else if(record.value().toString.contains("WARN")){
//            val json2=mapper.readValue("[{\"level\":\"WARN\"},{\"amount\":1},{\"message\":\""+record.value().toString.replaceAll("[^A-Za-z0-9 ]", "")+"\"}]", classOf[List[(String, Any)]])
//            ruleEngineActor ! ("input" ,json2)
//            println("WARN MESSAGE "+record.value())
//          }
//
//          else if(record.value().toString.contains("ERROR")){
//            val json3=mapper.readValue("[{\"level\":\"ERROR\"},{\"amount\":1},{\"message\":\""+record.value().toString.replaceAll("[^A-Za-z0-9 ]", "")+"\"}]", classOf[List[(String, Any)]])
//            ruleEngineActor ! ("input" ,json3)
//            println("ERROR MESSAGE "+record.value())
//          }
//          else if(record.value().toString.contains("INFO")){
//            val json3=mapper.readValue("[{\"level\":\"INFO\"},{\"amount\":1},{\"message\":\""+record.value().toString.replaceAll("[^A-Za-z0-9 ]", "")+"\"}]", classOf[List[(String, Any)]])
//            ruleEngineActor ! ("input" ,json3)
//            println("INFO MESSAGE "+record.value())
//          }
//          else {
//            val json3=mapper.readValue("[{\"level\":\"UNFORMATTED\"},{\"amount\":1},{\"message\":\""+record.value().toString.replaceAll("[^A-Za-z0-9 ]", "")+"\"}]", classOf[List[(String, Any)]])
//            ruleEngineActor ! ("input" ,json3)
//            println("UNFORMATTED MESSAGE "+record.value())
//          }
          val x=mapper.readValue(record.value().toString,classOf[List[Map[String, Any]]])
          println(x)
          ruleEngineActor ! ("input" ,x)
          Thread.sleep(200)
        }
      }
      println("\n\n\n*****Actor starts\n\n\n")
    }
  }
}
