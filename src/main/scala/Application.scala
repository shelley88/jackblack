import java.util

import akka.actor.{ActorRef, ActorSystem, Props}
import api.WebServer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import kamon.Kamon
import kamon.prometheus.PrometheusReporter
import ruleengine.{KafkaReaderActor, KafkaWriterActor, RuleEngineActor}
//import com.playtech.common.eventstream.client.api.event.{Event, EventFilter, EventFilters, EventHeader, Filter}
//import com.playtech.common.eventstream.client.api.stream.{ConsumableStream, StreamListener, Streams}

import scala.io.Source


object Application extends App {

  Kamon.addReporter(new PrometheusReporter())
println(args(0))
  if (args(0)==1){//timer
    println("TIMER")
    println("%%%%%%%%%%%%%%%%%%%%%%"+"lala log(debug) log(info)".replaceAll(".*(debug|info).*","{\"$1\":1}"))
    val model=Source.fromResource("RuleEngineTimer/model.json").getLines.fold("")(_ + _)// BlackJackPro/model.json
    val functions=Source.fromResource("RuleEngineTimer"+ "/functions.json").getLines.fold("")(_ + _)
    val rulesIn=Source.fromResource("RuleEngineTimer"+ "/rules.json").getLines.fold("")(_ + _)
//    val runner=Source.fromResource("RuleEngineTimer"+ "/runner.json").getLines.fold("")(_ + _)


    var system: ActorSystem = ActorSystem("SchedulerSystem")
    val writer:ActorRef= system.actorOf(Props(new KafkaWriterActor))
    val rules:ActorRef = system.actorOf(Props(new RuleEngineActor("/user/RuleEngineActor" + "/Model",writer)))
     val reader:ActorRef= system.actorOf(Props(new KafkaReaderActor(rules)))

    val mapper = new ObjectMapper
    mapper.registerModule(DefaultScalaModule)

    rules ! mapper.readValue(model, classOf[Map[String, Any]])
    rules ! mapper.readValue(functions, classOf[Map[String, Any]])("functions")
    rules ! mapper.readValue(rulesIn, classOf[Map[String, Any]])("rules")
//    rules ! mapper.readValue(runner, classOf[Map[String, Any]])("runner")

//    reader ! ("start","test",Map("bootstrap.servers"-> "PLAINTEXT://localhost:9092","key.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","value.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","group.id"-> "something"),".*(debug|info).*","{\"$1\":1}")
      reader ! ("start","ukraine_ums_kievimsdev1_java_log",Map("bootstrap.servers"-> "PLAINTEXT://172.31.223.245:6667","key.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","value.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","group.id"-> "ukraine_ums_kievimsdev1_blacksails","security.protocol"-> "SASL_PLAINTEXT","sasl.mechanism"-> "PLAIN","auto.offset.reset"-> "earliest","sasl.jaas.config"-> "org.apache.kafka.common.security.plain.PlainLoginModule required\nusername=\"kafka\"\npassword=\"kafka\";"),"","")


    WebServer.rulesActor = rules
  }else{
    println("RULEENGINE")
    println("%%%%%%%%%%%%%%%%%%%%%%"+"lala log(debug) log(info)".replaceAll(".*(debug|info).*","{\"$1\":1}"))
    val model=Source.fromResource("RuleEngine/model.json").getLines.fold("")(_ + _)// BlackJackPro/model.json
    val functions=Source.fromResource("RuleEngine"+ "/functions.json").getLines.fold("")(_ + _)
    val rulesIn=Source.fromResource("RuleEngine"+ "/rules.json").getLines.fold("")(_ + _)
    val runner=Source.fromResource("RuleEngine"+ "/runner.json").getLines.fold("")(_ + _)


    var system: ActorSystem = ActorSystem("SchedulerSystem")
    val writer:ActorRef= system.actorOf(Props(new KafkaWriterActor))
    val rules:ActorRef = system.actorOf(Props(new RuleEngineActor("/user/RuleEngineActor" + "/Model",writer)))
    val reader:ActorRef= system.actorOf(Props(new KafkaReaderActor(rules)))

    val mapper = new ObjectMapper
    mapper.registerModule(DefaultScalaModule)

    rules ! mapper.readValue(model, classOf[Map[String, Any]])
    rules ! mapper.readValue(functions, classOf[Map[String, Any]])("functions")
    rules ! mapper.readValue(rulesIn, classOf[Map[String, Any]])("rules")
    rules ! mapper.readValue(runner, classOf[Map[String, Any]])("runner")

//    reader ! ("start","test",Map("bootstrap.servers"-> "PLAINTEXT://localhost:9092","key.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","value.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","group.id"-> "something"),".*(debug|info).*","{\"$1\":1}")
      reader ! ("start","ukraine_ums_kievimsdev1_java_log",Map("bootstrap.servers"-> "PLAINTEXT://172.31.223.245:6667","key.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","value.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","group.id"-> "ukraine_ums_kievimsdev1_blacksails","security.protocol"-> "SASL_PLAINTEXT","sasl.mechanism"-> "PLAIN","auto.offset.reset"-> "earliest","sasl.jaas.config"-> "org.apache.kafka.common.security.plain.PlainLoginModule required\nusername=\"kafka\"\npassword=\"kafka\";"),"","")

    WebServer.rulesActor = rules
  }




//  reader ! ("start","ukraine_ums_kievimsdev1_java_log",Map("bootstrap.servers"-> "PLAINTEXT://172.31.223.245:6667","key.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","value.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","group.id"-> "ukraine_ums_kievimsdev1_blacksails","security.protocol"-> "SASL_PLAINTEXT","sasl.mechanism"-> "PLAIN","auto.offset.reset"-> "earliest","sasl.jaas.config"-> "org.apache.kafka.common.security.plain.PlainLoginModule required\nusername=\"kafka\"\npassword=\"kafka\";"),"","")
//  reader ! ("start","test",Map("bootstrap.servers"-> "PLAINTEXT://localhost:9092","key.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","value.deserializer"-> "org.apache.kafka.common.serialization.StringDeserializer","group.id"-> "something"),".*(debug|info).*","{\"$1\":1}")


  WebServer.startServer("localhost", 8080)


}
