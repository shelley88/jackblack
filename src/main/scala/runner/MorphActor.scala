package runner

import akka.actor.{Actor, ActorRef}
import akka.pattern.ask
import akka.util.Timeout
import engine.rules.GenericLambda
import shapeless.TypeCase

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class MorphActor(modelPath: ActorRef) extends Actor{
  val ListOfStrings    = TypeCase[List[String]]
  val MapOfStringAny = TypeCase[Map[String,_]]

  implicit val timeout = Timeout(5 seconds)
  def receive  ={
    case ("add",ListOfStrings(a),b:Any, MapOfStringAny(c)) => {
      val send = sender()
      val thing = modelPath ? "getModel"
      thing.onComplete{
        case Success(value) => {
          val v =GenericLambda.addElement(value.asInstanceOf[Map[String,Any]],value.asInstanceOf[Map[String,Any]],a,a,b)
          val v2 =modelPath ? v
          v2.onComplete{
            case Success(value) => send ! value
            case Failure(exception) => send ! exception
          }
        }
        case Failure(exception) => send ! exception
      }


    }
    case ("addWithoutFlatten",ListOfStrings(a),b:Any, MapOfStringAny(c)) => {
      val send = sender()
      val thing = modelPath ? "getModel"
      thing.onComplete{
        case Success(value) => {
          val v =GenericLambda.addElementWithoutFlatten(value.asInstanceOf[Map[String,Any]],value.asInstanceOf[Map[String,Any]],a,a,b)
          val v2 =modelPath ? v
          v2.onComplete{
            case Success(value) => send ! value
            case Failure(exception) => send ! exception
          }
        }
        case Failure(exception) => send ! exception
      }

    }
    case ("remove",ListOfStrings(a),b:Any, MapOfStringAny(c)) => {
      val send = sender()
      val thing = modelPath ? "getModel"
      thing.onComplete{
        case Success(value) =>{
          val v=GenericLambda.removeElement(value.asInstanceOf[Map[String,Any]],value.asInstanceOf[Map[String,Any]],a,a,b)
          val v2 =modelPath ? v
          v2.onComplete{
            case Success(value) => send ! value
            case Failure(exception) => send ! exception
          }
        }
        case Failure(exception) => send ! exception
      }

    }
    case ("removeFromPosition",ListOfStrings(a),b:Int, MapOfStringAny(c)) => {
      val send = sender()
      val thing = modelPath ? "getModel"
      thing.onComplete{
        case Success(value) =>{
          val v=GenericLambda.removeElementFromPosition(value.asInstanceOf[Map[String,Any]],value.asInstanceOf[Map[String,Any]],a,a,b)
          val v2 =modelPath ? v
          v2.onComplete{
            case Success(value) => send ! value
            case Failure(exception) => send ! exception
          }
        }
        case Failure(exception) => send ! exception
      }

    }
    case ("change",ListOfStrings(a),b:Any, MapOfStringAny(c)) => {
      println("change"+" "+a+" "+b+" "+ c)
      val send = sender()
      val thing = modelPath ? "getModel"
      thing.onComplete{
        case Success(value) =>{
          val v =GenericLambda.changeElement(value.asInstanceOf[Map[String,Any]],a,b)
          val v2 =modelPath ? v
          v2.onComplete{
            case Success(value) => send ! value
            case Failure(exception) => send ! exception
          }
        }
        case Failure(exception) => send ! exception
      }

    }

    case ("merge", MapOfStringAny(model),MapOfStringAny(input)) =>
    case b => sender() ! "morphActor not matching "+b
  }
}
