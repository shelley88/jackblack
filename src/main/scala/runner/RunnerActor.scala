package runner
import akka.pattern.ask
import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout
import engine.rules.{ModelActor, RuleActor}
import shapeless.TypeCase

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class RunnerActor(modelPath:String) extends Actor{
  val ListOfMap = TypeCase[List[Map[String,_]]]
  val ListOfMapOfMap = TypeCase[List[Map[String,Map[String,_]]]]
  val MapOfStringAny = TypeCase[Map[String,_]]
  val MapOfMap = TypeCase[Map[String,Map[String,_]]]
  val ListOfString = TypeCase[List[String]]
  implicit val timeout = Timeout(15 seconds)
  val modelActor: ActorRef=context.actorOf(Props(new ModelActor),name = "Model")
  val morph: ActorRef = context.actorOf(Props(new MorphActor(modelActor)),"Morph")

  import context._

  def receive= setModel

  def setModel:Receive ={
    case MapOfStringAny(a) => {

      modelActor ! a
      become(setFunctions)
    }
    case b => println("WRONG")
  }
  def setFunctions:Receive={
    case MapOfStringAny(a) => {
      become(setRules(a))
    }
  }

  def setRules(functions:Map[String,Any]):Receive ={
    case MapOfStringAny(a) => {
      val ruleActors=a.map(p => {
        val actor= actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2,functions)
        actor
      })
      become(setRunner(functions,ruleActors.toList))
    }
    case b => println("6not matching type "+b)
  }

  def setRunner(functions:Map[String,Any], ruleActors:List[ActorRef]):Receive ={
    case MapOfMap(rules) =>{
      val runnerActors=rules.filter(_._2.keySet.contains("rule")).map{ p=>
        val actor=actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2("rule") ,functions)
        actor
      }
      val stopActors=rules.filter(_._2.keySet.contains("stop-condition")).map{ p=>
        val actor=actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2("stop-condition") ,functions)
        actor
      }
      become(receiveWithParam(functions,rules,ruleActors,runnerActors.toList,stopActors.toList))
    }

  }
  def receiveWithParam(functions:Map[String,Any],rules:Map[String,Map[String,Any]],rulesActors:List[ActorRef],runnerActors:List[ActorRef],stopActors:List[ActorRef]):Receive ={
    case ("modifyRunner",MapOfMap(runner)) =>{
      runnerActors.filter(actor=> runner.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",runner(actor.path.name)("rule"))})
      stopActors.filter(actor=> runner.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",runner(actor.path.name)("stop-condition"))})

      val newRules=rules.map(rule => if(runner.contains(rule._1))(rule._1 -> runner(rule._1))  else (rule._1 -> rule._2))

      become(receiveWithParam(functions,newRules,rulesActors,runnerActors,stopActors))
    }
    case ("modifyFunctions",MapOfStringAny(runner)) => {
      val newFunctions=functions.map(rule => if(runner.contains(rule._1))(rule._1 -> runner(rule._1))  else (rule._1 -> rule._2))

      runnerActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      stopActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,runnerActors,stopActors))
    }
    case ("modifyRules",MapOfStringAny(runner)) =>{
      rulesActors.filter(actor=> runner.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",runner(actor.path.name))})
    }
    case ("addRunner",MapOfMap(runner)) =>{
      val newRunner = runner.filter(actor => actor._2.keySet.contains("rule")).map{
        actor => val newActor=actorOf(Props(new RuleActor(modelActor)),name = actor._1)
        newActor ! (actor._2("rule") ,functions)
        newActor
      }
      val newStop = runner.filter(actor => actor._2.keySet.contains("stop-condition")).map{
        actor => val newActor=actorOf(Props(new RuleActor(modelActor)),name = actor._1)
          newActor ! (actor._2("stop-condition") ,functions)
          newActor
      }

      val newRules=runner.map(rule => rule._1 -> rule._2)

      become(receiveWithParam(functions,rules ++ newRules,rulesActors,runnerActors ++ newRunner,stopActors ++ newStop))
    }
    case ("addFunctions",MapOfStringAny(runner)) =>{
      val newFunctions=functions ++ runner

      runnerActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      stopActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,runnerActors,stopActors))
    }
    case ("addRules",MapOfStringAny(runner)) =>{
      val newActors=runner.map(rule => {
        val actor =actorOf(Props(new RuleActor(modelActor)),name = rule._1)
        actor ! (rule._2,functions)
        actor
      })
      become(receiveWithParam(functions,rules,rulesActors ++ newActors.toList,runnerActors,stopActors))
    }
    case ("deleteRunner",ListOfString(runner)) =>{
      val (runnerActorDeleted,runnerActorKept) =runnerActors.partition(actor=> runner.contains(actor.path.name))
      val (stopActorsDeleted,stopActorKept) =stopActors.partition(actor=> runner.contains(actor.path.name))

      val newRules=rules.filterNot(rule => runner.contains(rule._1))

      runnerActorDeleted.foreach(actor => {system.stop(actor);context.watch(actor)})
      stopActorsDeleted.foreach(actor => {system.stop(actor);context.watch(actor)})

      become(receiveWithParam(functions,newRules,rulesActors,runnerActorKept,stopActorKept))
    }
    case ("deleteFunctions",ListOfString(runner)) =>{
      val newFunctions=functions.filterNot(rule => runner.contains(rule._1))

      runnerActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      stopActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,runnerActors,stopActors))
    }
    case ("deleteRules",ListOfString(runner)) =>{
      val (deletedActors,keptActors)=rulesActors.partition(p=> runner.contains(p.path.name))
      deletedActors.map(actor => {system.stop(actor);context.watch(actor);actor})
      become(receiveWithParam(functions,rules,keptActors,runnerActors,stopActors))
    }
    case ("run simulation") =>  {
      val send =sender()
      val thing = modelActor ? "getModel"
      val model = Await.result(thing.mapTo[Map[String,Any]],1000 millis)
      val w= runnerActors.map(p=> {p ? ("run",model)})
      val future=Future.sequence(w)
      val wait =Await.result(future,5000 millis)
      wait.foreach(p=> {
        if (p.asInstanceOf[Tuple2[String,Boolean]]._2) {
          val q=rules(p.asInstanceOf[Tuple2[String,Boolean]]._1)
          q("side-effects") match {
            case MapOfStringAny(a) => println("should be a list side-effects")
            case ListOfMap(b) => {
              val fut =q("side-effects").asInstanceOf[List[Map[String,Any]]].map(f => {
                val path = f("path").toString.split('.').toList
                val value = f("value")

                val act =actorOf(Props(new RuleActor(modelActor)))
                act ! (value , functions)
                val result = act ? ("run",model)
                val v=Await.result(result,20000 millis)
                context.stop(act)

                val typeOp = f("type")
                val v2=morph ? (typeOp, path, v.asInstanceOf[Tuple2[Any,Any]]._2,model)
                val p=Await.result(v2,10000 millis)
              })
            }
          }
        }
      })

      //HERE WE CAN CREATE A NEW ACTOR STATE AND WILL BE MUCH CLEARER
      val thing2 = modelActor ? "getModel"
      val model2 = Await.result(thing2.mapTo[Map[String,Any]],1000 millis)

      val w1= stopActors.map(p=> {p ? ("run",model2)})
      Future.sequence(w1).onComplete {
        case Success(value) => {
          val bool =value.asInstanceOf[List[Tuple2[String,Boolean]]].forall(j=>j._2)
          if(!bool){
            val ret =self ? "run simulation"
            val value = Await.result(ret,20000 millis)
            send ! value
          }else{
            send ! value
          }
        }
        case Failure(exception) => println("error in stop conditions")
      }
    }
    case MapOfStringAny(a) => {
      val send =sender()
      context.actorSelection(self.path.address+modelPath) ! a
    }
    case "getModel" => {
      val send =sender()
      val model =context.actorSelection(self.path.address+modelPath) ? "getModel"
      model.onComplete{
        case Success(value) => send ! value
        case Failure(exception) => send ! exception
      }

    }
    case "run" => {
      val send =sender()
      val thing = modelActor ? "getModel"
      val model = Await.result(thing,1000 millis)
      val w= rulesActors.map(p=> {p ? ("run",model)})
      Future.sequence(w).onComplete {
        case Success(a) => send ! a
        case Failure(b) => send ! b
      }
    }
    case c => println("error in runner actor"+c)
  }

}
