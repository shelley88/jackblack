package runner

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import engine.rules.{ModelActor, RuleActor}
import runner.{ChooseActionActor, MorphActor}
import shapeless.TypeCase

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

class ActionActor(modelPath:String) extends Actor{
  val ListOfMap = TypeCase[List[Map[String,_]]]
  val ListOfMapOfMap = TypeCase[List[Map[String,Map[String,_]]]]
  val MapOfStringAny = TypeCase[Map[String,_]]
  val MapOfMap = TypeCase[Map[String,Map[String,_]]]
  val ListOfString = TypeCase[List[String]]
  implicit val timeout = Timeout(4000 seconds)
  val modelActor: ActorRef=context.actorOf(Props(new ModelActor),name = "Model")
  val morph: ActorRef = context.actorOf(Props(new MorphActor(modelActor)),"Morph")

  import context._

  def receive= setModel

  def setModel:Receive ={
    case MapOfStringAny(a) => {

      modelActor ! a
      become(setFunctions)
    }
    case b => println("WRONG "+b)
  }
  def setFunctions:Receive={
    case MapOfStringAny(a) => {
      become(setRules(a))
    }
  }

  def setRules(functions:Map[String,Any]):Receive ={
    case MapOfStringAny(a) => {
      val ruleActors=a.map(p => {
        val actor= actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2,functions)
        actor
      })
      become(setAction(functions,ruleActors.toList))
    }
    case b => println("6not matching type "+b)
  }

  def setAction(functions:Map[String,Any], ruleActors:List[ActorRef]):Receive ={
    case MapOfMap(rules) =>{
      val send = sender()
      println("SENDER "+ send)
      val actionActors=rules.filter(_._2.keySet.contains("rule")).map{ p=>
        val actor=actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2("rule") ,functions)
        actor
      }
      val decisionActionActors=rules.filter(_._2.keySet.contains("decisionValue")).map{ p=>
        val actor=actorOf(Props(new RuleActor(modelActor)),name = p._1+"decision")
        actor ! (p._2("decisionValue") ,functions)
        actor
      }
      val stopActors=rules.filter(_._2.keySet.contains("stop-condition")).map{ p=>
        val actor=actorOf(Props(new RuleActor(modelActor)),name = p._1)
        actor ! (p._2("stop-condition") ,functions)
        actor
      }
      become(receiveWithParam(functions,rules,ruleActors,actionActors.toList,stopActors.toList,decisionActionActors.toList,"auto"))
    }
  }
  def receiveWithParam(functions:Map[String,Any],rules:Map[String,Map[String,Any]],rulesActors:List[ActorRef],actionActors:List[ActorRef],stopActors:List[ActorRef],decisionActionActors:List[ActorRef],whoDecides:String):Receive ={
    case "player" => {
      become(receiveWithParam(functions,rules,rulesActors,actionActors,stopActors,decisionActionActors,"player"))
    }
    case ("modifyAction",MapOfMap(action)) =>{
      actionActors.filter(actor=> action.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",action(actor.path.name)("rule"))})
      stopActors.filter(actor=> action.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",action(actor.path.name)("stop-condition"))})

      val newRules=rules.map(rule => if(action.contains(rule._1))(rule._1 -> action(rule._1))  else (rule._1 -> rule._2))

      become(receiveWithParam(functions,newRules,rulesActors,actionActors,stopActors,decisionActionActors,whoDecides))
    }
    case ("modifyFunctions",MapOfStringAny(action)) => {
      val newFunctions=functions.map(rule => if(action.contains(rule._1))(rule._1 -> action(rule._1))  else (rule._1 -> rule._2))

      actionActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      stopActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,actionActors,stopActors,decisionActionActors,whoDecides))
    }
    case ("modifyRules",MapOfStringAny(action)) =>{
      rulesActors.filter(actor=> action.contains(actor.path.name)).foreach(actor => {actor ! ("modifyRule",action(actor.path.name))})
    }
    case ("addAction",MapOfMap(action)) =>{
      val newAction = action.filter(actor => actor._2.keySet.contains("rule")).map{
        actor => val newActor=actorOf(Props(new RuleActor(modelActor)),name = actor._1)
        newActor ! (actor._2("rule") ,functions)
        newActor
      }
      val newStop = action.filter(actor => actor._2.keySet.contains("stop-condition")).map{
        actor => val newActor=actorOf(Props(new RuleActor(modelActor)),name = actor._1)
          newActor ! (actor._2("stop-condition") ,functions)
          newActor
      }

      val newRules=action.map(rule => rule._1 -> rule._2)

      become(receiveWithParam(functions,rules ++ newRules,rulesActors,actionActors ++ newAction,stopActors ++ newStop,decisionActionActors,whoDecides))
    }
    case ("addFunctions",MapOfStringAny(action)) =>{
      val newFunctions=functions ++ action

      actionActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      stopActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,actionActors,stopActors,decisionActionActors,whoDecides))
    }
    case ("addRules",MapOfStringAny(action)) =>{
      val newActors=action.map(rule => {
        val actor =actorOf(Props(new RuleActor(modelActor)),name = rule._1)
        actor ! (rule._2,functions)
        actor
      })
      become(receiveWithParam(functions,rules,rulesActors ++ newActors.toList,actionActors,stopActors,decisionActionActors,whoDecides))
    }
    case ("deleteAction",ListOfString(action)) =>{
      val (actionActorDeleted,actionActorKept) =actionActors.partition(actor=> action.contains(actor.path.name))
      val (stopActorsDeleted,stopActorKept) =stopActors.partition(actor=> action.contains(actor.path.name))

      val newRules=rules.filterNot(rule => action.contains(rule._1))

      actionActorDeleted.foreach(actor => {system.stop(actor);context.watch(actor)})
      stopActorsDeleted.foreach(actor => {system.stop(actor);context.watch(actor)})

      become(receiveWithParam(functions,newRules,rulesActors,actionActorKept,stopActorKept,decisionActionActors,whoDecides))
    }
    case ("deleteFunctions",ListOfString(action)) =>{
      val newFunctions=functions.filterNot(rule => action.contains(rule._1))

      actionActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      stopActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})
      rulesActors.foreach(actor => {actor ! ("modifyFunctions",newFunctions)})

      become(receiveWithParam(newFunctions,rules,rulesActors,actionActors,stopActors,decisionActionActors,whoDecides))
    }
    case ("deleteRules",ListOfString(action)) =>{
      val (deletedActors,keptActors)=rulesActors.partition(p=> action.contains(p.path.name))
      deletedActors.map(actor => {system.stop(actor);context.watch(actor);actor})
      become(receiveWithParam(functions,rules,keptActors,actionActors,stopActors,decisionActionActors,whoDecides))
    }
    case ("run simulation")  => {
      val send = sender()
println("HELLO")
      val thing = modelActor ? "getModel"
      val model = Await.result(thing.mapTo[Map[String, Any]], 1000 millis)

      val rulesRun = rulesActors.map(p => {
        p ? ("run", model)
      })
      val rulesFuture = Future.sequence(rulesRun)
      val rulesResult = Await.result(rulesFuture, 10000 millis)

      val w = actionActors.map(p => {
        p ? ("run", model, rulesResult)
      })
      val future = Future.sequence(w)
      val wait = Await.result(future, 20000 millis)


      //get list of rules to be run
      val dec = wait.filter(p => p.asInstanceOf[(String, Boolean, Boolean)]._3)
      val decisionList = dec.filter(p => p.asInstanceOf[(String, Boolean, Boolean)]._2) //.foreach(a => a.asInstanceOf[(String,(Boolean,Number),Boolean)]._1)

      val decisionAct = actorOf(Props(new ChooseActionActor(modelActor)))

      println("DECISIONLIST    "+decisionList+"\nMODEL    "+model)

      //decision
      if (decisionList.nonEmpty) {
        val d = decisionActionActors.filter(p => decisionList.asInstanceOf[List[(String, Boolean, Boolean)]].filter(q => q._1 == p.path.name.replace("decision", "")).nonEmpty).map(p => {
          println("run "+p+" "+rulesResult+"  "+ model)
          p ? ("run", model, rulesResult)
        })
        //        val d=decisionActionActors.filter(p => {println("\\\\\\\\\\\\\\\\\\\\\\|"+p.path.name); decisionList.contains(p.path.name.toString.replace("decision",""))}).map(p=> {p ? ("run",model,rulesResult)})
        val dFut = Future.sequence(d)
        val decWait = Await.result(dFut.mapTo[List[(String, Any, Boolean)]], 10000 millis)

        val DECISION = decWait.filter(p => decisionList.asInstanceOf[List[(String, Boolean, Boolean)]].filter(q => q._1 == p._1.replace("decision", "")).nonEmpty)
        println("DECISION     "+DECISION)

        ///////
        //
        //      if player decision send to http also need to have a player or auto in become
        //
        ///////

        if(whoDecides.equals("auto")){
          val decisionWait = decisionAct ! ("decide", DECISION,wait,rulesResult,model,send)
        }
        else{
          println("SEND FOR DECIDE")
          send ! ("decide", DECISION,wait,rulesResult,model,send)
        }



//        DECISION GOES HERE

//        val decisionResult = Await.result(decisionWait.mapTo[List[String]], 10 second)
//
//        val decisionRun = wait.filter(p => decisionResult.contains(p.asInstanceOf[(String, Boolean, Boolean)]._1))
//
//        //player or strategy side effects
//        decisionRun.foreach(p => {
//          val q = rules(p.asInstanceOf[(String, Boolean, Boolean)]._1)
//          q("side-effects") match {
//            case MapOfStringAny(a) => println("should be a list side-effects")
//            case ListOfMap(b) => {
//              val fut = q("side-effects").asInstanceOf[List[Map[String, Any]]].map(f => {
//                val path = f("path").toString.split('.').toList
//                val value = f("value")
//
//                val act = actorOf(Props(new RuleActor(modelActor)))
//                act ! (value, functions)
//                val result = act ? ("run", model,rulesResult)
//                val v = Await.result(result, 20000 millis)
//                context.stop(act)
//
//                val typeOp = f("type")
//                val v2 = morph ? (typeOp, path, v.asInstanceOf[(Any, Any,Any)]._2, model)
//                val p = Await.result(v2, 10000 millis)
//              })
//            }
//          }
//        })
      }
      else {
        val thing4 = modelActor ? "getModel"
        val model4 = Await.result(thing4.mapTo[Map[String, Any]], 1000 millis)
        //
        //Normal side effects to be run
        wait.foreach(p => {
          if (!p.asInstanceOf[(String, Any, Boolean)]._3 && p.asInstanceOf[(String, Boolean, Boolean)]._2) {
            val q = rules(p.asInstanceOf[(String, Boolean, Boolean)]._1)
            q("side-effects") match {
              case MapOfStringAny(a) => println("should be a list side-effects")
              case ListOfMap(b) => {
                val fut = q("side-effects").asInstanceOf[List[Map[String, Any]]].map(f => {
                  val path = f("path").toString.split('.').toList
                  val value = f("value")

                  val act = actorOf(Props(new RuleActor(modelActor)))
                  act ! (value, functions)
                  val result = act ? ("run", model4, rulesResult)
                  val v = Await.result(result, 20000 millis)
                  context.stop(act)

                  val typeOp = f("type")
                  val v2 = morph ? (typeOp, path, v.asInstanceOf[(Any, Any, Any)]._2, model4)
                  val p = Await.result(v2, 10000 millis)
                })
              }
            }
          }
        })

        //HERE WE CAN CREATE A NEW ACTOR STATE AND WILL BE MUCH CLEARER
        val thing2 = modelActor ? "getModel"
        val model2 = Await.result(thing2.mapTo[Map[String, Any]], 1000 millis)

        val w1 = stopActors.map(p => {
          p ? ("run", model2, rulesResult)
        })
        Future.sequence(w1).onComplete {
          case Success(value) => {
            val bool = value.asInstanceOf[List[(String, Boolean, Boolean)]].forall(j => j._2)
            if (!bool) {
              //            val act = system.actorOf(Props(new ActionActor(modelPath)))

              //            act ! ("set",functions,rules,rulesActors,actionActors.toList,stopActors.toList,decisionActionActors.toList)

              val ret = self ? "run simulation"
              //            v.map(p=> (self.path.name,p)).pipeTo(s)
              ret.map(p => p).pipeTo(send)
              //            val valu = Await.result(ret,20000 millis)
              //            println("SENDER  Call "+send+"    "+sender()+"    "+valu)
              //            send ! valu
            } else {
              println("SENDER  " + send + "    " + sender() + "    " + value)
              send ! value
            }
          }
          case Failure(exception) => println("error in stop conditions")
        }
      }
  }
    case ("decide",ListOfString(decisions),wait,rulesResult,model,s)=>{
      println("DECIDED "+decisions)
//      println("SENDER   "+send)

      val send = if (s.isInstanceOf[ActorRef]){
        s
      }else{
        sender()
      }

//      val decisionResult = Await.result(decisionWait.mapTo[List[String]], 10 second)

      val decisionRun = wait.asInstanceOf[List[(String,Boolean,Boolean)]].filter(p => decisions.contains(p._1))

      println("DECISION RUN "+ decisionRun + " "+wait)


      //player or strategy side effects
      decisionRun.foreach(p => {
        val q = rules(p.asInstanceOf[(String, Boolean, Boolean)]._1)
        q("side-effects") match {
          case MapOfStringAny(a) => println("should be a list side-effects")
          case ListOfMap(b) => {
            val fut = q("side-effects").asInstanceOf[List[Map[String, Any]]].map(f => {
              val path = f("path").toString.split('.').toList
              val value = f("value")

              val act = actorOf(Props(new RuleActor(modelActor)))
              act ! (value, functions)
              val result = act ? ("run", model,rulesResult)
              val v = Await.result(result, 20000 millis)
              context.stop(act)

              val typeOp = f("type")
              val v2 = morph ? (typeOp, path, v.asInstanceOf[(Any, Any,Any)]._2, model)
              val p = Await.result(v2, 10000 millis)
            })
          }
        }
      })
      val thing4 = modelActor ? "getModel"
      val model4 = Await.result(thing4.mapTo[Map[String,Any]],1000 millis)

      //Normal side effects to be run
      wait.asInstanceOf[List[(String,Boolean,Boolean)]].foreach(p=> {
        if (!p.asInstanceOf[(String,Any,Boolean)]._3 && p.asInstanceOf[(String,Boolean,Boolean)]._2) {
          val q=rules(p.asInstanceOf[(String,Boolean,Boolean)]._1)
          q("side-effects") match {
            case MapOfStringAny(a) => println("should be a list side-effects")
            case ListOfMap(b) => {
              val fut =q("side-effects").asInstanceOf[List[Map[String,Any]]].map(f => {
                val path = f("path").toString.split('.').toList
                val value = f("value")

                val act =actorOf(Props(new RuleActor(modelActor)))
                act ! (value , functions)
                val result = act ? ("run",model4,rulesResult)
                val v=Await.result(result,20000 millis)
                context.stop(act)

                val typeOp = f("type")
                val v2=morph ? (typeOp, path, v.asInstanceOf[(Any,Any,Any)]._2,model4)
                val p=Await.result(v2,10000 millis)
              })
            }
          }
        }
      })

      //HERE WE CAN CREATE A NEW ACTOR STATE AND WILL BE MUCH CLEARER
      val thing2 = modelActor ? "getModel"
      val model2 = Await.result(thing2.mapTo[Map[String,Any]],1000 millis)

      val w1= stopActors.map(p=> {p ? ("run",model2,rulesResult)})
      Future.sequence(w1).onComplete {
        case Success(value) => {
          val bool = value.asInstanceOf[List[(String, Boolean, Boolean)]].forall(j => j._2)
          if (!bool) {
            //            val act = system.actorOf(Props(new ActionActor(modelPath)))

            //            act ! ("set",functions,rules,rulesActors,actionActors.toList,stopActors.toList,decisionActionActors.toList)

            val ret = self ? "run simulation"
            //            v.map(p=> (self.path.name,p)).pipeTo(s)
            ret.map(p => p).pipeTo(send.asInstanceOf[ActorRef])
            //            val valu = Await.result(ret,20000 millis)
            //            println("SENDER  Call "+send+"    "+sender()+"    "+valu)
            //            send ! valu
          } else {
            println("SENDER  " + send + "    " + sender() + "    " + value)
            send.asInstanceOf[ActorRef] ! value
          }
        }
        case Failure(exception) => println("error in stop conditions")
      }
    }
    case MapOfStringAny(a) => {
      val send =sender()
      context.actorSelection(self.path.address+modelPath) ! a
    }
    case "getModel" => {
      val send =sender()
      val model =context.actorSelection(self.path.address+modelPath) ? "getModel"
      model.onComplete{
        case Success(value) => send ! value
        case Failure(exception) => send ! exception
      }

    }
    case "run" => {
      val send =sender()
      val thing = modelActor ? "getModel"
      val model = Await.result(thing,1000 millis)
      val w= rulesActors.map(p=> {p ? ("run",model)})
      Future.sequence(w).onComplete {
        case Success(a) => send ! a
        case Failure(b) => send ! b
      }
    }
    case c => println("error in action actor"+c)
  }

}
