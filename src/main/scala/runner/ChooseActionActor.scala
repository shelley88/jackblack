package runner

import akka.actor.{Actor, ActorRef}
import akka.util.Timeout
import shapeless.TypeCase

import scala.concurrent.duration._

class ChooseActionActor(modelPath:ActorRef) extends Actor{
  val MapOfStringAny = TypeCase[Map[String,_]]
  val DoubleComp = TypeCase[List[(String,Double,Boolean)]]
  val IntComp = TypeCase[List[(String,Int,Boolean)]]
  val NumberComp = TypeCase[List[(String,Number,Boolean)]]
  implicit val timeout = Timeout(5 seconds)

  def receive= processRule

  def processRule:Receive = {
    //need to create rule to decide
    case ("decide",NumberComp(b),wait,rulesResult,model,send) => println("******\n** NUMBER for decisions "+b+"\n** the player should choose: "+b.sortBy(_._2.doubleValue()).reverse.head._1+"\n******");/*Thread.sleep(300000);*/sender ! ("decide",List(b.sortBy(_._2.doubleValue()).reverse.head._1.replace("decision","")),wait,rulesResult,model,send)


    case b => println("4not matching type choose chooseActor " + b); sender ! List("lala")
  }

}
