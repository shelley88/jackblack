# Black Sails / Jack Black

##Documentation
This project allows a user to create a game from scratch or load a default game and modify it on the fly, and run a simulation.

To do so you will need to read through these documents to understand how it works.

[Create a Game from scratch](/docs/pages/createGameFromScratch.md)

[Model](/docs/pages/model.md)

[Functions](/docs/pages/functions.md)

[Rules](/docs/pages/rules.md)

[Runner](/docs/pages/runner.md)

[Modify, Add and Delete; Rules, Runner and Functions](/docs/pages/modifyGame.md)

## Run this project

The easiest way is through IntelliJ, all you need to have installed in your machine is a valid JDK 8 or above should be OK, though it has been developed using 10.

Install the Scala plugin and import the project.

To run the server just run the `Application` class, and once it is running you can interact with it via HTTP, Postman is recommended as there are some postman collections included in the project as to have some initial interactions setup.

## How to work on this project

### Tests

There is a main UserStoryTest, that makes sure that a normal complete run works after changes, this will give 
you a heads up if a demo needs changing.

Unit Tests have to be created for every Actor, this is incredible important, as the system is very complex and 
bugs can be introduced very easily.

Coverage is calculated using __scoverage__ to see this report first run `sbt clean coverage test` to calculate the 
coverage and then `sbt coverageReport` to get the report, you will get a rundown in the terminal, but you can 
also navigate through one of the reports created, for example te html one can be opened in your browser.

Though there is no pipeline in place to check that the coverage stays at a certain level, it is up to the developer to keep this number high. Coverage should stay `over 90%` at all times as this will assure that the quality of the project is quite high.
It is also very important to mention in this point that the coverage report is a good way to make sure there are enough tests, it is up to the developer to make sure all cases are tested.
This is a very complicated project and all happy paths, corner cases and error paths should be adequately tested. and as much of this should be tested at a unit test level, allowing integration and automated tests to only test what they are supposed to, that is integration and regression.

### Keeping Jira and BitBucket linked

It is really important to know what has been added to the project and why, for this reason we are going to reference the ticket and the Merge Requests.

When a merge request is created this should have the Jira ticket link added into the description, and once the Merge Request is created this link should be added to the Jira ticket in a comment. 

To push any new code a feature branch should be created in your local Git repository. For this we are going to use `bug/${jira-ticket}` for bugs and `feature/${jira-ticket}` for everything else where __${jira-ticket}__ should be __JBL-{ticket-number}-{ticket-title-in-small-caps-and-separated-by-hyphen}__
When creating merging the Merge Request, this should always squash the commits and delete the branch, unless there is an extremely good reason.

It would probably be very interesting to develop some git hooks to automate some of this for the future.

### Tagging
At the moment all versioning of the code is done on a demo to demo basis, as there is no production or staging environment.

In the future tags should be used to control versions of the project in environments and docs. Ideally there will be a list of environments and the version sitting in it
in Confluence, these versions will have a change log linked to it from Jira, and a link to the documentation for that specific version, making it easier for people to switch between environments.

Ideally when a tag is created a version will automatically be crested in Jira along with the change log, and the version of the documents should be published in Confluence along with the version added to a list of versions of docs automatically.

### JIRA
https://portal-jira.playtech.corp/secure/RapidBoard.jspa?projectKey=JBL&rapidView=11293&view=planning

For now we are only going to be using 3 ticket types, Task, Story and Bug. For Task or Bug there is not much explanation needed.

If the ticket type is a Story there will need to be some obligatory sub-tasks created: development, unit-tests, manual-testing, performance, automated-testing and documentation, this will oblige the developer to keep a decent code quality per ticket.

For now estimations are not being made, the Backlog should have the first few tickets in order, and the tickets are assigned when it's development starts.

### Monitoring
Install Prometheus and Grafana


### Demos
It is extremely important in a project like this that there are concise points in the code where everything works, 
at the moment this doesn't necessarily mean tests work, in demo1 and demo2 the tests are not functioning, 
but from this point on it will not be acceptable.

The docs, with the code will need to be in the source code so as to have a decent versioning system.

The demos are also kept in the source code, we have postman collections and graphs and presentations, 
as well as the a tag in the code which will allow us to navigate between different versions and have an 
up to date doc and presentation

- __Demo 1__
    
    Tagged in the project as `demo1` there is a [presentation](docs/demos/presentations/Demo1.pdf) and a json that can be imported to [postman](/docs/demos/postman/demo1.postman_collection.json)

    This is the very first checkpoint for this project, it is a simple POC to show how this project can grow into a very useful game design tool.

- __Demo 2__

    Tagged in the project as `demo2` there is a [presentation](docs/demos/presentations/Demo2.pdf) and a json that can be imported to [postman](/docs/demos/postman/demo2.postman_collection.json)

    This presentation walks through a simplified BlackJack game design, and gives a high level architecture view of the project.

- __Demo 3__

    Tagged in the project as `demo3` there is a json that can be imported to [postman](docs/demos/postman/demo3.postman_collection.json), there is no presentation for this as it just runs through the rules for the game designed.

    This presentation runs through a version of ScratchCards game design and also outlines the possibility of modifying the game on the fly, allowing us to build on a template game.

- __Demo 4__
    
    This demo will probably have very little to show as SD is in the way and most of the time has been used on documentation, though some performance features should be introduced.
    
- __Demo 5__
    
    Tagged in the project as `demo5`. This demo will be a big changing point, so a lot will be explained, DB, and decisions are the 2 biggest points.
    
    Also a real BJ will be included. [postman](docs/demos/postman/demo5.postman_collection.json) [presentation](docs/demos/presentations/Demo5.pdf)
    
- __Demo 6__
    
    First demo to stake holders quick recap of functionality plus decision making. [postman](docs/demos/postman/demo6.postman_collection.json) [presentation](docs/demos/presentations/Demo6.pdf)
    
- __Demo 7__

    Demo Rule Engine use case [postman](docs/demos/postman/demo7.postman_collection.json) [presentation](docs/demos/presentations/Demo7.pdf)
                                  
                                                                                       
    
__CASSANDRA__
  
    install docker, don't use brew in mac it's more complicated
    
    docker pull cassandra
    
    docker run -p 9042:9042 --name cassy -d cassandra:latest
    
    docker start cassy
    
    docker run -it --link cassy:cassandra --rm cassandra cqlsh cassandra
    
    in cqlsh run the cql code that is in post.cql
     

__KAFKA__

    Recomended
    brew tap AdoptOpenJDK/openjdk
    brew cask install adoptopenjdk8
    brew install kafka
    change in /usr/local/etc/kafka/server.properties  listeners=PLAINTEXT://:9092 --> listeners=PLAINTEXT://localhost:9092
    zkServer start
    kafka-server-start /usr/local/etc/kafka/server.properties
    kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
    kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test2
    kafka-console-producer --broker-list localhost:9092 --topic test
    kafka-console-consumer --bootstrap-server localhost:9092 --topic test2 --from-beginning
    
--

    Docker
    
    docker pull spotify/kafka
    
    docker run -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=172.17.0.2 --env ADVERTISED_PORT=9092 spotify/kafka
    
    -- open two extra terminal tabs
    
    -- run in both
    docker exec -it <container name> /bin/bash (get name by running docker ps)
    cd /opt/kafka_2.11-0.10.1.0/bin/
    
    --consumer
    export ZOOKEEPER=172.17.0.2:2181
    ./kafka-console-consumer.sh --zookeeper $ZOOKEEPER --topic test
    
    --producer
    export KAFKA=172.17.0.2:9092
    ./kafka-console-producer.sh --broker-list $KAFKA --topic test
    
    --watch messages sent from producer to consumer
      
       
__PROMETHEUS AND GRAFANA__
    
    brew install prometheus
    have prometheuls.yml in root
    prometheus / brew services start prometheus
    http://localhost:9090/graph
    
    brew install grafana
    brew services start grafana
    http://localhost:3000
    setup datasource to prometheus on localhost:9090 

    
example scrape duration:

goto localhost:9090/graph and run scrape_duration_seconds

got to localhost:3000

add prometheus as datasource 

![dataSource](docs/images/Screen Shot 2019-01-25 at 17.18.36.png)

 create new dashboard with graph, and edit graph like so

![graph](docs/images/Screen Shot 2019-01-25 at 17.15.21.png?raw=true "Graph")

