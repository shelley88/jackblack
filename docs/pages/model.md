## Model

This section is meant to be a full introduction to how to create and how Models work.

In the future this should be separated into a definition of the model, building blocks and then finally the initial model.

The model is a simple JSON file sent to an Actor and saved as a `Map[String,_]` this is a very simple way of treating the Data that will be changed in the future to use Generic Types, and will also use a definition of a model and building blocks to simplify work especially for front end developers.
This will allow them to use the definition to show users possible constructors for the data, and using building blocks will allow re-usability of data.

An example model could be something like this: 

```
{"hand1":1,
"hand2":[{"val1":1,"val2":2},{"val1":1,"val2":2}],
"table":[{"val1":1,"val2":2},{"val1":1,"val2":2}]}
```
