## Rules

This section is meant to be a full introduction to how to create and how Rules work.

An Actor is created per rule, when run, they return List with Tuples consisting of the name of the rule and the result of that particular run.

In the future these should be usable from the runner as well.

An example could be:

```
{"rules":{
"isFunc":{"type":"function","function":"func1","input":{"lala":1}},	
"isWinner1":{"type":"value","value":"Equals"}
}}
```