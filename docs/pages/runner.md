## Runner

This section is meant to be a full introduction to how to create and how Runners work.

An Actor is created per rule and per stop-condition plus a list of all rules as to access the side-effects when needed, when run, it loops over the rules and then after the stop-conditions repeating until all stop-conditions are true.
When running a rule if the conditions are met the side-effects are run.

An example could be:

```
{"runner":{
	"make1-3inhand1":{
		"rule":{
			"type":"operation",
			"operator":"Equals",
			"parameters":[{"type":"value","value":1},{"type":"path","path":"hand1"}]},
			"side-effects":[{"type":"change","path":"hand1","value":{"type":"value","value":3}}] },
	"stop":{
		"stop-condition":{
			"type":"operation",
			"operator":"Equals",
			"parameters":[{"type":"value","value":3},{"type":"path","path":"hand1"}]}}}}
```