## Add Modify and Delete Rules, Functions and Runner
This functionality will allow a user to load an existing game and modify it's rules.

For business this will be a great tool to play around with to have an idea of what the RTP could look like without needing
to specifically ask a mathematician, hopefully speeding up the game design process.

### API

The API consist of 9 new end-points:

- __/modifyRules__

    This takes a Map with the same format as __/setRules__ and substitutes each of the rules in the original rules with the new ones

- __/addRules__

    This takes a Map with the same format as __/setRules__ and adds each of the rules to the original rules

- __/deleteRules__

    This takes a list of string and deletes from the list of rules the ones listed

- __/modifyRunner__

    This takes a Map with the same format as __/setRunner__ and substitutes each of the runner rules in the original runner rules with the new ones

- __/addRunner__

    This takes a Map with the same format as __/setRunner__ and adds each of the runner rules to the original runner rules

- __/deleteRunner__

    This takes a list of string and deletes from the list of runner rules the ones listed

- __/modifyFunctions__

    This takes a Map with the same format as __/setFunctions__ and substitutes each of the functions in the original functions with the new ones

- __/addFunctions__

    This takes a Map with the same format as __/setFunctions__ and adds each of the functions to the original functions

- __/deleteFunctions__

    This takes a list of string and deletes from the list of functions the ones listed

### Example

For example the `BlackJack-simple` could be loaded, and then change some of the rules, this example has incorrect and
incomplete rules, for example if both dealer and player are bust the outcome is nobody wins, we can override the original rule:

```
...
"both-bust": {
      "rule": {
        "type": "operation",
        "operator": "Equals",
        "parameters": [
          {
            "type": "operation",
            "operator": "GreaterThan",
            "parameters": [
              {
                "type": "path",
                "path": "dealerPoints.soft"
              },
              {
                "type": "value",
                "value": 21
              }
            ]
          },
          {
            "type": "operation",
            "operator": "Equals",
            "parameters": [
              {
                "type": "path",
                "path": "gameStage"
              },
              {
                "type": "value",
                "value": 4
              }
            ]
          },
          {
            "type": "operation",
            "operator": "GreaterThan",
            "parameters": [
              {
                "type": "path",
                "path": "playerPoints.soft"
              },
              {
                "type": "value",
                "value": 21
              }
            ]
          },
          {
            "type": "value",
            "value": true
          }
        ]
      },
      "side-effects": [
        {
          "type": "change",
          "path": "gameStage",
          "value": {
            "type": "value",
            "value": 5
          }
        },
        {
          "type": "change",
          "path": "gameNumber",
          "value": {
            "type": "operation",
            "operator": "Add",
            "parameters": [
              {
                "type": "value",
                "value": 1
              },
              {
                "type": "function",
                "function": "getGameNumber",
                "input": {}
              }
            ]
          }
        }
      ]
    }
    ...
```

And then substitute it with the corrected version through the REST API in `/modifyRunner`:

```
{rules:{
"both-bust": {
      "rule": {
        "type": "operation",
        "operator": "Equals",
        "parameters": [
          {
            "type": "operation",
            "operator": "GreaterThan",
            "parameters": [
              {
                "type": "path",
                "path": "dealerPoints.soft"
              },
              {
                "type": "value",
                "value": 21
              }
            ]
          },
          {
            "type": "operation",
            "operator": "Equals",
            "parameters": [
              {
                "type": "path",
                "path": "gameStage"
              },
              {
                "type": "value",
                "value": 4
              }
            ]
          },
          {
            "type": "operation",
            "operator": "GreaterThan",
            "parameters": [
              {
                "type": "path",
                "path": "playerPoints.soft"
              },
              {
                "type": "value",
                "value": 21
              }
            ]
          },
          {
            "type": "value",
            "value": true
          }
        ]
      },
      "side-effects": [
        {
          "type": "change",
          "path": "gameStage",
          "value": {
            "type": "value",
            "value": 5
          }
        },
        {
          "type": "change",
          "path": "gameNumber",
          "value": {
            "type": "operation",
            "operator": "Add",
            "parameters": [
              {
                "type": "value",
                "value": 1
              },
              {
                "type": "function",
                "function": "getGameNumber",
                "input": {}
              }
            ]
          }
        },
        {
          "type": "change",
          "path": "dealerWins",
          "value": {
          "type": "operation",
          "operator": "Add",
          "parameters": [
            {
              "type": "value",
              "value": 1
            },
            {
              "type": "function",
              "function": "getDealerWins",
              "input": {}
            }
          ]
        }
       }
      ]
    }
    }}
```

As you can see all that has been added is an extra change in the side effects:
```
...
{
          "type": "change",
          "path": "dealerWins",
          "value": {
          "type": "operation",
          "operator": "Add",
          "parameters": [
            {
              "type": "value",
              "value": 1
            },
            {
              "type": "function",
              "function": "getDealerWins",
              "input": {}
            }
          ]
        }
       }
       ...
```

This simply makes it so that if both player and dealer are bust, the money doesn't dissapear but stays with the dealer, which is what would happen in real life.

