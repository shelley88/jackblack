## Create a Game from Scratch

There are two ways of creating and running a new game through the API and via the resource files (until DB is in place)

If you want to load a pre-made game and modify it at the moment only the API can be used.

For this we need to understand the four types of input the simulator needs to run:

- __Functions__

    This contains a collection of helper functions that can be called from other functions, the runner or the rules. [Functions](/docs/pages/functions.md)
  
- __Model__

    This contains the initial model for the game. [Model](/docs/pages/model.md)
  
- __Rules__

    This contains a collection of rules, that when they are run each give an output depending on the current state of the model. [Rules](/docs/pages/rules.md)

- __Runner__

    This contains a collection of runner rules, that also if requirements are met side-effects are applied to the model and stop-conditions which are run after the rules and once all stop-conditions are true the simulation stops. [Runner](/docs/pages/runner.md)


### Create and run game with resource files (will be changed to DB and modify will be added)

For this we are going to ony need to use 1 endpoint:

- __/runSimulation__ 

   This will take the resource files, described immediately ahead, and load run and create sessions automatically, allowing for concurrent runs of the game.

   This endpoint takes 2 headers:

   - `concurrent` : this takes a number and describes the number of concurrent runs that you want to run.
   - `simulation` : this takes a string with the name of the resource folder ${game}, described immediately below, where the game has been designed.

   For this endpoint we are going to need to create a new file in the main resource file of the project and give it a descriptive name ${game}, (this will be changed when a DB is put in place and it will load from the DB).
   inside this folder ${game} we need to create 4 JSON files `functions.json` , `model.json` , `rules.json` and `runner.json` .

   At this point the rules file is basically never used but needs to be created as Errors are not being handled.

   The output will be a list with the model after the simulation has run for each concurrent run.
 
### Create, run and interact with game through API

For this we are going to need the following endpoints: the numbered ones have to be run before the rest and in that order.

All of these endpoints, take one header, `session` which is a number, and has to be unique per session

1. __/setModel__

    This endpoint takes the model and sets the modelActor to the model given. This endpoint also sets the session.

2. __/setFunctions__

    Takes the collection of Functions and creates adds them to an internal list so they can be accessed to create an Actor when needed in the Rules or the Runner

3. __/setRules__

    Takes the collection of Rules and creates an Actor per Rule.

4. __/setRunner__

    Takes the collection of Runner Rules and creates an Actor per Runner Rule and Stop Condition.

- __/runRules__

    Runs all the rules once.

- __/runRunner__

    Runs all the Runner Rules applying the side-effects while the Stop Conditions are not all true.

- __/updateModel__

    Changes the Model for the one given.

- __/getModel__

    Returns the current state of the Model.

- __/sessions__

    Returns a list of current Sessions.

- __/stop__

    Stops the session for the header given. Stops all actors.
