## Functions

This section is meant to be a full introduction to how to create and how Functions work.

Functions are kept in a List[Map[String,_]]  with each entry in the List being a Function. These Functions take inputs and give a result, they can be used by Rules and Runners.

An example could be:

```
{"functions":{
  "shuffle":{"type":"operation","operator":"Shuffle","parameters":[{"type":"path","path":"shoe"}]},
  "takeFirst":{"type":"operation","operator":"GetFirst","parameters":[{"type":"path","path":"shoe"}]},
  "func1":{"type":"input","input":"lala"}
}}
```