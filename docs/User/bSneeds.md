#Black Sails
## Current internal work
- performance tests - 2 weeks
- user docs - ongoing
- test with specific data and rules - 2 weeks

## FE integration with BetBuddy
- need to understand what the integration should look like as in will this be an iFrame inside an existing project
- probably will consist in websocket connection to BE or a simple node.js kafka reader
- will need somebody with experience with data visualization libraries like d3 or similar

## FE rules creator
- some extra work is needed by FE developer
- some bugs have been found
- functions and rules need to be added
- some styling is needed


## Model specification with Avro
- this work is a very simple though probably a longish piece of work, it will consist in integrating Avro schema in the creation of the model as to have model safety, but it will also need a FE component where you will be able to construct this model

## Infrastructure and Integration 

- machine sizing will be determined in performance tests
    - around 2 million events in deposits and withdrawals daily this will only need one
    - around 20 million spins or 50 million events in GPAS daily this may need a cluster TBD
- need to know how many events will be read in use cases
- need machines to run test and production environments
- need access to working Event Stream with testing capability
- useful to have a global view of all event types and amount daily for different licensees and environments
