#Performance tests
[Amazon EC2 instance list with prices](https://aws.amazon.com/ec2/instance-types/t3/?sc_icampaign=Adoption_Campaign_pac-edm-2019-ec2_t3-site_merch-awareness-general-awareness_t3_instance-1_2_up&sc_ichannel=ha&sc_icontent=awssm-2460&sc_ioutcome=Enterprise_Digital_Marketing&sc_iplace=2up&trk=ha_a131L000005vR8cQAE~ha_awssm-2460&trkCampaign=pac-edm-2019-ec2_t3-webpage-t3_instance_announcement)

The absolute minimum number of machines needed are 2 to perform a decent battery of performance tests.

- One for the rule Tortuga, Kafka and the feeder, most probably an t3.small will be ok.

- The rule engine can be tested on 3 types, medium, large and xlarge as 2xlarge is probably not needed for the current use case

The first test to be run will be feed as much data as possible and see the breaking point on each size of machine, this will give us a pretty decent idea of memory vs CPU needs. 

The second test will be run in the most reasonable size of instance for about a week to check for memory leeks and equivalent

##Performance tests costs

Pricing for different scenarios, in dollars:

- first week (3.3424)
    - one week of stress max stress for feeder machine, one day for each BS type plus one day for setup, turned off in the night time 8 x 4 small instance
        0.0209 x 8 x 4 = 0.6688
    - two day medium instance for BS, one day test plus one day setup, 8 x 2 x 0.0418 = 0.6688
    - one day large instance for BS, 8 x 0.0835 = 0.668
    - one day xlarge instance for BS, 8 x 0.167 = 1.336
    
- second week, one of the following:

    - one week small feeder plus one week medium BS; 7 x 24 (0.0209 + 0.0418) = 10.5336
    - one week small feeder plus one week large BS; 7 x 24 (0.0209 + 0.0835) = 17.5392
    - one week small feeder plus one week xlarge BS; 7 x 24 (0.0209 + 0.167) = 31.5672
    
Worst case scenario 31.5672 + 1.336 + 0.6688 x 3 = **34.9096** dollars

If a medium machine is needed for secondary machine (most probably not) the pricing would change as follows:

- first week (4.0096)

   - one week of stress max stress for feeder machine, one day for each BS type plus one day for setup, turned off in the night time 8 x 4 small instance
        0.0418 x 8 x 4 = 1.336
   - two day medium instance for BS, one day test plus one day setup, 8 x 2 x 0.0418 = 0.6688
   - one day large instance for BS, 8 x 0.0835 = 0.668
   - one day xlarge instance for BS, 8 x 0.167 = 1.336
    
- second week:

   - one week small feeder plus one week medium BS; 7 x 24 (0.0418 + 0.0418) = 14.0448
   - one week small feeder plus one week large BS; 7 x 24 (0.0418 + 0.0835) = 21.0504
   - one week small feeder plus one week xlarge BS; 7 x 24 (0.0418 + 0.167) = 35.0784
   
And that would leave a bill of **39.088** dollars, in a worst case scenario
    
    
##Performance tests design

As stated before we are going to need 2 machines 

### First machine
The set up consists of a few necessary steps 

Before we start we will need to create a data feeder, that will be usable for both stages of the performance tests, send messages at full throttle for the first step and send a steady load for the second this will take no longer than a day

We will also need to setup machines and install kafka, feeder, node and such into the feeder machine, this will take no longer than a day

### Second machine

We will need to install and prep the 3 BS machines this will take no longer than a day

### Running tests

3 days will be needed for running the full throttle tests and documenting what their breaking points are one for every size of machine.

Another week of running the 2 machines and some light checks and data collection will need to be done, but the most probable is that I can do other stuff in the mean time

###Important data collection

####Memory

####CPU

####Disk


## Larger Tests
In this part we can explore the performance and sizing for a larger data set

There is a possibility that we could have 100 million events a day, and this is an amazing opportunity to demonstrate the real value of a project like BS.

we have 1.5 million deposits and nearly 500k withdrawals per day

Under 20 million spins in Corosin

45 million operations in GPAS a day

so we will need to see what the architecture needs to look like to deal with 100 million events a day 