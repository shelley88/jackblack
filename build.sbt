name := "BlackSails"

version := "0.1"

scalaVersion := "2.12.6"
//resolvers += Resolver.mavenLocal
//resolvers in Global := Resolver.mavenLocal
//"Local Maven" at Path.userHome.asFile.toURI.toURL + ".m2/repository"
resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

libraryDependencies +="com.typesafe.akka" %% "akka-http"   % "10.1.3"
libraryDependencies +="com.typesafe.akka" %% "akka-stream" % "2.5.12"

libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.6"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.14"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.14" % Test
libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % "10.1.3"
libraryDependencies +="com.chuusai" %% "shapeless" % "2.3.3"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "0.10.1.0"
libraryDependencies ++= Seq(
  "io.kamon" %% "kamon-core" % "1.1.0",
  "io.kamon" %% "kamon-logback" % "1.0.0",
  "io.kamon" %% "kamon-akka-2.5" % "1.0.1",
  "io.kamon" %% "kamon-prometheus" % "1.0.0",
  "io.kamon" %% "kamon-zipkin" % "1.0.0"
)



//libraryDependencies ++= Seq(
//  "com.playtech.common" % "eventstream_client_kafka" % "19.4.2" exclude("org.slf4j", "slf4j-log4j12"),
//  "com.playtech.eventstream.model" % "eventstream_model_common" % "19.8.1",
//"com.playtech.eventstream.model" % "eventstream_model_descriptor" % "19.8.1",
//  "com.playtech.eventstream.model" % "eventstream_model_eventstream" % "19.8.1"
//)

//coverageEnabled.in(Test, test) := false
libraryDependencies += "io.getquill" %% "quill-cassandra" % "2.5.4"
//libraryDependencies += "org.cassandraunit" % "cassandra-unit" % "3.1.3.2" % Test

resolvers += Resolver.typesafeIvyRepo("releases")

libraryDependencies += "com.lightbend" %% "emoji" % "1.2.1"

//// No need to run tests while building jar
//test in assembly := {}
//// Simple and constant jar name
//assemblyJarName in assembly := s"blacksails_2.12-0.1.jar"
//// Merge strategy for assembling conflicts
//assemblyMergeStrategy in assembly := {
//  case PathList("reference.conf") => MergeStrategy.concat
//  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
//  case _ => MergeStrategy.first
//}